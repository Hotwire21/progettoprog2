# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: javaMuseo
# Generation Time: 2018-10-29 14:22:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table commenti
# ------------------------------------------------------------

DROP TABLE IF EXISTS `commenti`;

CREATE TABLE `commenti` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueID` varchar(100) DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  `feel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `commenti` WRITE;
/*!40000 ALTER TABLE `commenti` DISABLE KEYS */;

INSERT INTO `commenti` (`id`, `uniqueID`, `text`, `feel`)
VALUES
	(1,'c1506t','Bellissimo posto, invito chiunque a visitarlo!','LIKE'),
	(8,'c1506t','ljl','LIKE'),
	(9,'c1506t','awsretygui','LIKE'),
	(10,'c1506t','qwedas','LIKE'),
	(11,'c1506t','dsadqwdasd','LIKE'),
	(12,'c1506t','dasdsadasdsad','DISLIKE'),
	(13,'c1506t','ma chi cazzu dici','DISLIKE'),
	(14,'c1506t','ma nnatibinni a travagghiari','DISLIKE'),
	(15,'c1506t','ma che bella vista','LIKE'),
	(16,'c1506t','gentile e cortese','LIKE'),
	(17,'c1506t','adasdasads','DISLIKE'),
	(18,'c1506t','cscdasfasfdsafasdfasdfasdfsadfsadfsdasdasdfdsfs','LIKE'),
	(19,'c1506t','ciao gianni','LIKE'),
	(20,'G15nluca','i bagni sono spochi, lavateli meglio...','DISLIKE'),
	(21,'G15nluca','le statue sono tutte nude...che vergogna!','LIKE'),
	(22,'G15nluca','opdakpdaskopdsaopdasokpads','DISLIKE'),
	(23,'c1506t','adsadsdaads','LIKE'),
	(24,'c1506t','asddas','LIKE'),
	(25,'c1506t','saasdcdwqeas','DISLIKE');

/*!40000 ALTER TABLE `commenti` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orari
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orari`;

CREATE TABLE `orari` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `giorno` varchar(100) DEFAULT NULL,
  `openM` varchar(100) DEFAULT NULL,
  `closeM` varchar(100) DEFAULT NULL,
  `openP` varchar(100) DEFAULT NULL,
  `closeP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `orari` WRITE;
/*!40000 ALTER TABLE `orari` DISABLE KEYS */;

INSERT INTO `orari` (`id`, `giorno`, `openM`, `closeM`, `openP`, `closeP`)
VALUES
	(1,'Lunedì','07','11','16','20'),
	(2,'Martedì','11','13','16','21'),
	(3,'Mercoledì','15','21','10','12'),
	(4,'Giovedì','09','13','16','20'),
	(5,'Venerdì','16','21','11','12'),
	(6,'Sabato','08','12','14','18');

/*!40000 ALTER TABLE `orari` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table prenotazioni
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prenotazioni`;

CREATE TABLE `prenotazioni` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` varchar(100) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `num_bambini` varchar(100) DEFAULT NULL,
  `num_adulti` varchar(100) DEFAULT NULL,
  `num_ridotto` varchar(100) DEFAULT NULL,
  `prezzo_tot` int(11) DEFAULT NULL,
  `giorno` varchar(100) DEFAULT NULL,
  `data_pre` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `prenotazioni` WRITE;
/*!40000 ALTER TABLE `prenotazioni` DISABLE KEYS */;

INSERT INTO `prenotazioni` (`id`, `id_utente`, `nome`, `num_bambini`, `num_adulti`, `num_ridotto`, `prezzo_tot`, `giorno`, `data_pre`)
VALUES
	(46,'c1506t','Cristian Costa','1','0','0',5,'2018-08-17','2018-08-03 18:30:33'),
	(48,'c1506t','Cristian Costa','5','6','4',25,'2018-08-23','2018-08-11 11:52:15'),
	(49,'c1506t','Cristian Costa','1','0','1',5,'2018-09-12','2018-08-31 20:06:47'),
	(50,'c1506t','Cristian Costa','4','2','0',40,'2018-10-04','2018-09-26 11:39:24'),
	(51,'G15nluca','Gianluca Lombardo','1','0','0',6,'2018-10-26','2018-10-09 17:08:45'),
	(52,'G15nluca','Gianluca Lombardo','0','0','4',32,'2018-10-17','2018-10-09 17:14:01'),
	(53,'c1506t','Cristian Costa','1','0','0',6,'2018-10-28','2018-10-09 20:07:40'),
	(54,'c1506t','Cristian Costa','1','0','0',6,'2018-10-18','2018-10-09 20:33:06'),
	(55,'c1506t','Cristian Costa','1','0','0',6,'2018-12-16','2018-10-09 20:48:50');

/*!40000 ALTER TABLE `prenotazioni` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table prezzi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prezzi`;

CREATE TABLE `prezzi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `prezzo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `prezzi` WRITE;
/*!40000 ALTER TABLE `prezzi` DISABLE KEYS */;

INSERT INTO `prezzi` (`id`, `nome`, `prezzo`)
VALUES
	(1,'Bambino',6),
	(2,'Adulto',15),
	(3,'Ridotto',8);

/*!40000 ALTER TABLE `prezzi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table utenti
# ------------------------------------------------------------

DROP TABLE IF EXISTS `utenti`;

CREATE TABLE `utenti` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueID` varchar(100) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `acc_key` varchar(100) DEFAULT NULL,
  `sec_key` varchar(100) DEFAULT NULL,
  `permesso` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `utenti` WRITE;
/*!40000 ALTER TABLE `utenti` DISABLE KEYS */;

INSERT INTO `utenti` (`id`, `uniqueID`, `nome`, `acc_key`, `sec_key`, `permesso`)
VALUES
	(1,'c1506t','Cristian Costa','3091316179-58WoG1b5Ce9dbMGj0nembFkZ1J9HIDhkoLICdHq','n8UFoduiRjdwrp754Td1tseo3cqbDwldMbdReIUTd72lF','1'),
	(2,'turiddu','Zappa Turi','3091316179-58WoG1b5Ce9dbMGj0nembFkZ1J9HIDhkoLICdHq','n8UFoduiRjdwrp754Td1tseo3cqbDwldMbdReIUTd72lF','2'),
	(3,'melino','Melo Vota','3091316179-58WoG1b5Ce9dbMGj0nembFkZ1J9HIDhkoLICdHq','n8UFoduiRjdwrp754Td1tseo3cqbDwldMbdReIUTd72lF','2'),
	(4,'G15nluca','Gianluca Lombardo','4387476388-p68TwsLvYTRsVIlgaMdxHULJomKOBZWHFa11ZP4','BTPGNXkIDw8uEr1tOjUHkcYm2FzEQ3PuRGx18ky4gyLaN','1');

/*!40000 ALTER TABLE `utenti` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
