package adminmuseo;

import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class conf {
    static String id;
    static String utente;
    static String dir;
    static String server;
    static String port;
    static String name;
    static String acc_key;
    static String sec_key;    
    static String pin;
    static String nome;
    static String foto;
    static long a;
    static long idtw;
     // LEGGERE FILE DI CONFIGURAZIONE
   
    static {
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("src/projectFiles/conf.json"));
            JSONObject jsonObject = (JSONObject) obj;
            name = (String) jsonObject.get("name");
            dir = (String) jsonObject.get("directory");                        
            server = (String) jsonObject.get("server_address");
            port = (String) jsonObject.get("server_port");
            acc_key = (String) jsonObject.get("access_token");                        
            sec_key = (String) jsonObject.get("secret_token");           
        } catch (IOException | ParseException ex) {
            Logger.getLogger(conf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
