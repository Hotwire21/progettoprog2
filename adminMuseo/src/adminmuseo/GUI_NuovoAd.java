package adminmuseo;

import java.io.IOException;
import java.net.ProtocolException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_NuovoAd extends javax.swing.JFrame {
    public GUI_NuovoAd() throws IOException, ProtocolException, ParseException, NoSuchAlgorithmException, NoSuchPaddingException, NoSuchPaddingException {  
        try {
            initComponents();
            JSONObject user = new JSONObject();
            user.put("idtw", conf.idtw);
            request req = new request();
            req.postRequest(conf.server, conf.port, "tokenServer", user);
            jButton1.setVisible(false);
            jButton2.setVisible(false);
            listaUt.setVisible(false);
            listaAd.setVisible(false);
            jLabel1.setVisible(false);
            jLabel2.setVisible(false);
            //richiesta per ottenere gli utenti, la prima richiesta va su tutti gli utenti inseriti nel db
            JSONObject returnJson = req.getRequest(conf.server, conf.port, "getUser");
            JSONArray array = (JSONArray)returnJson.get("utenti");
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);
                if ((name.get("permesso").equals("2"))){
                    listaUt.addItem((String) name.get("id"));
                }else{
                    listaAd.addItem((String) name.get("id"));
                }                
            }
        } catch (Exception ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        benvenuto = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        listaUt = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        listaAd = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        benvenuto1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        benvenuto.setText("Per modificare i permessi è neccessario inserire il codice di sicurezza");

        back.setText("<<");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        jLabel1.setText("Id utenti");

        jButton1.setText("Conferma Nuovo Account Admin");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        listaUt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { }));
        listaUt.setLightWeightPopupEnabled(false);
        listaUt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaUtActionPerformed(evt);
            }
        });

        jLabel2.setText("Id admin");

        listaAd.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { }));
        listaAd.setLightWeightPopupEnabled(false);
        listaAd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaAdActionPerformed(evt);
            }
        });

        jButton2.setText("Rimuovi Account Admin");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton3.setText("Conferma Codice Sicurezza");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        benvenuto1.setText("di sicurezza ricevuto come messaggio privato sul tuo account twitter.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addComponent(Titolo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(listaUt, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(listaAd, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 89, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(benvenuto)
                    .addComponent(benvenuto1))
                .addGap(87, 87, 87))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(benvenuto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(benvenuto1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(listaUt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(listaAd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(134, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        //alla pressione del tasto si ritorna alla home page
        this.setVisible(false);
        GUI_Admin gui;
        try {
            gui = new GUI_Admin(0,1,conf.pin);
            gui.setVisible (true);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Grafici.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_backActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {

                JSONObject ris = new JSONObject();
                JSONObject data = new JSONObject();
                request ric = new request();
                String id=(String) listaUt.getSelectedItem();
                data.put("id",id);
                ris =ric.postRequest(conf.server, conf.port, "nuovoAdmin", data);
                int r=Integer.parseInt(String.valueOf(ris.get("ok")));
                this.setVisible(false);
                GUI_Admin ad = new GUI_Admin(r,1,"");
                ad.setVisible(true);
                                       
        } catch (IOException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void listaUtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaUtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_listaUtActionPerformed

    private void listaAdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaAdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_listaAdActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {

                JSONObject ris = new JSONObject();
                JSONObject data = new JSONObject();
                request ric = new request();
                String id=(String) listaAd.getSelectedItem();
                data.put("id",id);
                ris =ric.postRequest(conf.server, conf.port, "rimuoviAdmin", data);
                int r=Integer.parseInt(String.valueOf(ris.get("ok")));
                this.setVisible(false);
                GUI_Admin ad = new GUI_Admin(r,1,"");
                ad.setVisible(true);         
        } catch (IOException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            String tok=jTextField1.getText();
            JSONObject obj = new JSONObject();
            JSONObject returnJson = new JSONObject();
            obj.put("idtw", conf.idtw);
            obj.put("token",tok);
            request req = new request();
            returnJson=req.postRequest(conf.server, conf.port, "validateTokenServer", obj);
            String res=(String) returnJson.get("ok");
            if("2".equals(res)){
                this.setVisible(false);
                GUI_Admin ad= new GUI_Admin(3, 1, conf.pin);
                ad.setVisible(true);
            }else {
                jButton3.setVisible(false);
                jTextField1.setVisible(false);
                benvenuto.setText("Il codice inserito è valido, puoi procedere con le operazioni.");
                benvenuto1.setVisible(false);
                jLabel1.setVisible(true);
                jLabel2.setVisible(true);
                jButton1.setVisible(true);
                jButton2.setVisible(true);
                listaUt.setVisible(true);
                listaAd.setVisible(true);
            }
            
            
            
            
            
            
            
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(GUI_NuovoAd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed


    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton back;
    private javax.swing.JLabel benvenuto;
    private javax.swing.JLabel benvenuto1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox<String> listaAd;
    private javax.swing.JComboBox<String> listaUt;
    // End of variables declaration//GEN-END:variables
}
