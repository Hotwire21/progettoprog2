package adminmuseo;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.json.simple.parser.ParseException;

public class AdminMuseo {

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     * @throws java.io.FileNotFoundException
     * @throws org.json.simple.parser.ParseException
     */
    public static void main(String[] args) throws MalformedURLException, IOException, FileNotFoundException, ParseException, Exception {
        //Creazione chiavi di crittografia pubblica e privata
        GenerateKeys gk;
        try {
            gk = new GenerateKeys(1024);
            gk.createKeys();
            gk.writeToFile(conf.dir+"publicKey", gk.getPublicKey().getEncoded());
            gk.writeToFile(conf.dir+"privateKey", gk.getPrivateKey().getEncoded());
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //Invio richiesta al server per verificare la connessione
        URL obj = new URL(conf.server + ":" + conf.port + "/testConnessione");
        int a=0;
            try {
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();                
                con.setRequestMethod("GET");
                con.setRequestProperty("Accept-Language", "en-US");
                con.setRequestProperty("Content-Type", "application/json");                
                con.setDoOutput(true);                
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.write("".getBytes());
                wr.flush();
                wr.close();
                //Se il server è online procedo
                if (con.getResponseCode() == 200) {
                    //System.out.println("Connessione stabilita");
                    a=1;
                    exe(a);
                    keys();
                    GUI_Login gui = new GUI_Login();
                    gui.setVisible (true);                         
                }
            //Se il server è offline termino il programma
            } catch (IOException ex) {
                exe(a);
                ex.printStackTrace();
            }
    }
    
    static public void keys() throws MalformedURLException, ProtocolException, ProtocolException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{           
        //Richiesta al server della sua chiave pubblica
        URL obj1 = new URL(conf.server + ":" + conf.port + "/publicKey");            
        HttpURLConnection con1 = (HttpURLConnection) obj1.openConnection();                
        con1.setRequestMethod("GET");
        con1.setRequestProperty("Accept-Language", "en-US");             
        con1.setDoOutput(true);                
        DataOutputStream wr1 = new DataOutputStream(con1.getOutputStream());
        wr1.write("".getBytes());
        wr1.flush();
        wr1.close();
        //ottengo la risposta
        int dim_Response = con1.getContentLength();            
        DataInputStream in = new DataInputStream(con1.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        in.close();
        //scrive la chiave su di un file
        AsymmetricCryptography ac = new AsymmetricCryptography();
        File f =new File(conf.dir+"serverKey");
        ac.writeToFile(f, data);
 
        //Richiesta al server per postare la propria chiave pubblica
        URL obj2 = new URL(conf.server + ":" + conf.port + "/getAdminKey" );            
        HttpURLConnection con2 = (HttpURLConnection) obj2.openConnection();
        con2.setRequestMethod("POST");
        con2.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        File f2 =new File(conf.dir+"publicKey");
        byte[] myKey=ac.getFileInBytes(f2);
        con2.setDoOutput(true);
        DataOutputStream wr2 = new DataOutputStream(con2.getOutputStream());
        wr2.write(myKey);
        wr2.flush();
        wr2.close();                                                    
        int dim_Response2 = con2.getContentLength();            
        DataInputStream in2 = new DataInputStream(con2.getInputStream());                                    
        byte[] data2 = new byte[dim_Response2];            
        in2.readFully(data2);
        in2.close();  
        
    }
    //creazione di un frame usato per verificare la connessione che mostra una barra di avanzamento stato
    static public void exe (int a) throws FileNotFoundException, IOException, ParseException{                                                                                                                         
        final int MAX = 100;
        final JFrame frame = new JFrame("Museo di Messina");
         
        // creo progress bar
        final JProgressBar pb = new JProgressBar();
        JLabel testo=new JLabel();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        pb.setMinimum(0);
        pb.setMaximum(MAX);
        pb.setStringPainted(true);
 
        // aggiungo la progress bar al frame
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(pb);                       
        frame.getContentPane().add(testo); 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 80);
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);        
        frame.setVisible(true);                
        
        // aggiorno progress bar
        for (int i = 0; i <= MAX; i++) {
            final int currentValue = i;
            int wait = 40;            
            if (currentValue == 0){
                pb.setVisible (false);
                testo.setText ("Avvio del programma in corso...");
                wait = 2000;
            }
            if (currentValue == 1){
                pb.setVisible (true);
                testo.setText ("Caricamento informazioni...");
                }
            if (currentValue == 39){
                testo.setText ("Controllo connessione...");
                wait = 1500;
            }
            //la var a serve per la verifica dello stato della connessione
            if (currentValue == 61){
                if(a==0){
                    testo.setText ("Server offline! Terminazione programma!");
                    wait = 1000;                        
                }else{
                    testo.setText ("Caricamento impostazioni admin...");
                    wait = 700;
                }
            }
            if (currentValue == 99){
                if(a==0){
                    System.exit(0);                        
                }else{
                    testo.setText ("Caricamento GUI in corso...");
                    wait = 1000;
                }
            }                              
            try {
                SwingUtilities.invokeLater(() -> {
                    pb.setValue(currentValue);
                });
                java.lang.Thread.sleep(wait);
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(frame, e.getMessage());
            }            
        } 
        frame.setVisible(false);
    }
}
