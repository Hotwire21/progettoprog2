package adminmuseo;

import java.awt.Color;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.ButtonGroup;
import javax.swing.table.JTableHeader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_Utenti extends javax.swing.JFrame {
    public GUI_Utenti() {
       try {
        //System.out.println("Caricamento lista utenti!");
        initComponents();
        JTableHeader th = tabellaUs.getTableHeader();
            th.setBackground(Color.getHSBColor(0.6f, 0.6f, 0.6F ));
        ButtonGroup bg = new ButtonGroup();
            bg.add(all);
            bg.add(user);
            bg.add(admin);
            all.setContentAreaFilled(true);
            request req = new request();
            //richiesta per ottenere gli utenti, la prima richiesta va su tutti gli utenti inseriti nel db
            JSONObject returnJson = req.getRequest(conf.server, conf.port, "getUser");
            JSONArray array = (JSONArray)returnJson.get("utenti");
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);
                tabellaUs.setValueAt(name.get("id"), i, 0);
                tabellaUs.setValueAt(name.get("nome"), i, 1);
                if ((name.get("permesso").equals("2"))){  
                    tabellaUs.setValueAt("CLIENTE", i, 2);
                }else{
                    tabellaUs.setValueAt("ADMIN", i, 2);
                }
            }
            
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
        //run();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        benvenuto = new javax.swing.JLabel();
        benvenuto1 = new javax.swing.JLabel();
        all = new javax.swing.JRadioButton();
        user = new javax.swing.JRadioButton();
        admin = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabellaUs = new javax.swing.JTable();
        back = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        benvenuto.setText("Qui potrai visualizzare l'intera lista di utenti registrati al sito!");

        benvenuto1.setText("Seleziona la tipologia di utenti da ricercare");

        all.setSelected(true);
        all.setText("ALL");
        all.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allActionPerformed(evt);
            }
        });

        user.setText("UTENTI");
        user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userActionPerformed(evt);
            }
        });

        admin.setText("AMMINISTRATORI");
        admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminActionPerformed(evt);
            }
        });

        tabellaUs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "NOME", "PERMESSO"
            }
        ));
        tabellaUs.setToolTipText("");
        tabellaUs.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabellaUs.setAutoscrolls(false);
        tabellaUs.setEnabled(false);
        tabellaUs.setRequestFocusEnabled(false);
        tabellaUs.setRowSelectionAllowed(false);
        tabellaUs.setShowHorizontalLines(false);
        tabellaUs.setShowVerticalLines(false);
        tabellaUs.getTableHeader().setResizingAllowed(false);
        tabellaUs.getTableHeader().setReorderingAllowed(false);
        tabellaUs.setUpdateSelectionOnSort(false);
        tabellaUs.setVerifyInputWhenFocusTarget(false);
        tabellaUs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tabellaUsMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(tabellaUs);

        back.setText("<<");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(98, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(all)
                        .addGap(76, 76, 76)
                        .addComponent(user)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(admin))
                    .addComponent(benvenuto, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(90, 90, 90))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addComponent(Titolo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(benvenuto1))
                    .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(benvenuto)
                .addGap(97, 97, 97)
                .addComponent(benvenuto1)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(all)
                    .addComponent(user)
                    .addComponent(admin))
                .addGap(40, 40, 40)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void allActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allActionPerformed
        //System.out.println("---> Richiesta lista totale!");
        for(int i=0;i<10;i++){
           for(int j=0;j<3;j++){              
            tabellaUs.setValueAt("", i,j);
           }
        }
        request req = new request();
        JSONObject returnJson;
        try {
            returnJson = req.getRequest(conf.server, conf.port, "getUser");
            JSONArray array = (JSONArray)returnJson.get("utenti");
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);
                tabellaUs.setValueAt(name.get("id"), i, 0);
                tabellaUs.setValueAt(name.get("nome"), i, 1);
                if ((name.get("permesso").equals("2"))){  
                    tabellaUs.setValueAt("CLIENTE", i, 2);
                }else{
                    tabellaUs.setValueAt("ADMIN", i, 2);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }//GEN-LAST:event_allActionPerformed

    private void userActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userActionPerformed
        //System.out.println("---> Richiesta lista clienti!");
        for(int i=0;i<10;i++){
           for(int j=0;j<3;j++){              
            tabellaUs.setValueAt("", i,j);
           }
        }
        request req = new request();
        JSONObject returnJson;
        try {
            returnJson = req.getRequest(conf.server, conf.port, "getUser");
            JSONArray array = (JSONArray)returnJson.get("utenti");
            int j=0;
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);                
                if ((name.get("permesso").equals("2"))){                
                tabellaUs.setValueAt(name.get("id"), j, 0);
                tabellaUs.setValueAt(name.get("nome"), j, 1);
                tabellaUs.setValueAt("CLIENTE", j, 2);
                j++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_userActionPerformed

    private void adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adminActionPerformed
        //System.out.println("---> Richiesta lista admin!");
        for(int i=0;i<10;i++){
           for(int j=0;j<3;j++){              
            tabellaUs.setValueAt("", i,j);
           }
        }
        request req = new request();
        JSONObject returnJson;
        try {
            returnJson = req.getRequest(conf.server, conf.port, "getUser");
            JSONArray array = (JSONArray)returnJson.get("utenti");
            int j=0;
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);                
                if ((name.get("permesso").equals("1"))){                
                tabellaUs.setValueAt(name.get("id"), j, 0);
                tabellaUs.setValueAt(name.get("nome"), j, 1);
                tabellaUs.setValueAt("ADMIN", j, 2);
                j++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_adminActionPerformed

    private void tabellaUsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabellaUsMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tabellaUsMouseEntered

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        //alla pressione del tasto si ritorna alla home page
        this.setVisible(false);
        GUI_Admin gui;
        try {
            gui = new GUI_Admin(0,1,"");
            gui.setVisible (true);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Grafici.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_backActionPerformed


    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JRadioButton admin;
    private javax.swing.JRadioButton all;
    private javax.swing.JButton back;
    private javax.swing.JLabel benvenuto;
    private javax.swing.JLabel benvenuto1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabellaUs;
    private javax.swing.JRadioButton user;
    // End of variables declaration//GEN-END:variables
}
