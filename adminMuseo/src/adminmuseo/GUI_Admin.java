package adminmuseo;

import java.awt.Image;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_Admin extends javax.swing.JFrame {
    /**
     * Creates new form GUI_Admin
     * @param nuovoAdmin
     * @param login
     * @param pin
     * @throws java.net.ProtocolException
     * @throws java.net.MalformedURLException
     * @throws org.json.simple.parser.ParseException
     * @throws javax.crypto.NoSuchPaddingException
     * @throws javax.crypto.IllegalBlockSizeException
     * @throws java.io.UnsupportedEncodingException
     */
    @SuppressWarnings("empty-statement")
    public GUI_Admin(int nuovoAdmin,int login, String pin) throws ProtocolException, IOException, MalformedURLException, ParseException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, IllegalBlockSizeException, Exception, Exception, Exception, Exception, IllegalBlockSizeException, IllegalBlockSizeException, Exception, IllegalBlockSizeException {                
            initComponents();
            // il parametro nuovo admin serve per il pulsante
            //il parametro login serve per capire se la classe viene richiamata per la prima volta oppure a causa del tasto "BACK"
            //il parametro pin serve per passare il codice ricevuto da twitter tramite la GUI_LOGIN
            endAd.setVisible(false);
            if (nuovoAdmin==1){
                endAd.setVisible(true);
            }else if(nuovoAdmin==2){
                endAd.setText("Admin rimosso con successo!");
                endAd.setVisible(true);
            }else if (nuovoAdmin==3){
                endAd.setText("Il Codice inserito è errato!");
                endAd.setVisible(true);
            }
         
            
            if (login==0){
                //System.out.println("---> Login effettuato con successo!");
                JSONObject obj = new JSONObject();
                obj.put("pin", pin);
                JSONObject returnJson = new JSONObject();            
                //invio richiesta al server
                request log = new request();
                returnJson = log.postRequest(conf.server, conf.port, "loginAdmin", obj);
                conf.a=(long) returnJson.get("res");
                conf.nome = (String) returnJson.get("nome");
                conf.foto = (String) returnJson.get("foto");
                conf.id = (String) returnJson.get("id");
                conf.idtw = (long) returnJson.get("idtw");
            }
            if (conf.a==0){
                user.setText(conf.nome);
                Image image=null;
                URL url = new URL(conf.foto);
                image = ImageIO.read(url);
                pict.setIcon(new ImageIcon(image));
                error.setVisible(false);
                uscita.setVisible(false);
            }
            else if(conf.a==1)
            {
                error.setText("Siamo spiacenti ma non possiedi i permessi di amministratore");
                benvenuto.setVisible(false);
                user.setVisible(false);
                graph.setVisible(false);
                utenti.setVisible(false);
                admin.setVisible(false);
                upgrade.setVisible(false);
            }        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        benvenuto = new javax.swing.JLabel();
        user = new javax.swing.JLabel();
        pict = new javax.swing.JLabel();
        error = new javax.swing.JLabel();
        graph = new javax.swing.JButton();
        uscita = new javax.swing.JButton();
        utenti = new javax.swing.JButton();
        admin = new javax.swing.JButton();
        upgrade = new javax.swing.JButton();
        endAd = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        benvenuto.setText("Benvenuto admin, sei loggato come: ");

        error.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        error.setForeground(new java.awt.Color(255, 51, 51));

        graph.setText("Grafici");
        graph.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graphActionPerformed(evt);
            }
        });

        uscita.setText("Termina programma");
        uscita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uscitaActionPerformed(evt);
            }
        });

        utenti.setText("Utenti");
        utenti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                utentiActionPerformed(evt);
            }
        });

        admin.setText("Gestione Admin");
        admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminActionPerformed(evt);
            }
        });

        upgrade.setText("Modifiche");
        upgrade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upgradeActionPerformed(evt);
            }
        });

        endAd.setForeground(new java.awt.Color(255, 51, 51));
        endAd.setText("Nuovo admin inserito con successo!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(218, 218, 218)
                .addComponent(uscita)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(upgrade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(utenti, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(graph, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(admin, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(47, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(Titolo)
                        .addGap(142, 142, 142))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(benvenuto, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pict, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(45, 45, 45))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))))
            .addGroup(layout.createSequentialGroup()
                .addGap(191, 191, 191)
                .addComponent(endAd)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endAd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                .addComponent(pict, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(benvenuto)
                    .addComponent(user))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(error)
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(utenti, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(upgrade, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(graph, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(admin, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(uscita, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(74, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void graphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graphActionPerformed
        this.setVisible(false);
        GUI_Grafici gf;
        gf = new GUI_Grafici();
        gf.setVisible(true);        
    }//GEN-LAST:event_graphActionPerformed

    private void uscitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uscitaActionPerformed
        this.setVisible(false);        
        System.exit(0);
    }//GEN-LAST:event_uscitaActionPerformed

    private void utentiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_utentiActionPerformed
        this.setVisible(false);
        GUI_Utenti us = new GUI_Utenti();
        us.setVisible(true);
    }//GEN-LAST:event_utentiActionPerformed

    private void adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adminActionPerformed
        try {
            this.setVisible(false);
            GUI_NuovoAd us = new GUI_NuovoAd();
            us.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(GUI_Admin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Admin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Admin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_adminActionPerformed

    private void upgradeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upgradeActionPerformed
        this.setVisible(false);
        GUI_Modifiche mod = new GUI_Modifiche(0);
        mod.setVisible(true);
    }//GEN-LAST:event_upgradeActionPerformed
      

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton admin;
    private javax.swing.JLabel benvenuto;
    private javax.swing.JLabel endAd;
    private javax.swing.JLabel error;
    private javax.swing.JButton graph;
    private javax.swing.JLabel pict;
    private javax.swing.JButton upgrade;
    private javax.swing.JButton uscita;
    private javax.swing.JLabel user;
    private javax.swing.JButton utenti;
    // End of variables declaration//GEN-END:variables
}
