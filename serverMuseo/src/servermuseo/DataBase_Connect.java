package servermuseo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase_Connect {

    public Connection Connessione() {
        Connection conn = null;
        try {
            //Crea la connessione con database
            Class.forName("com.mysql.jdbc.Driver");  
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/javaMuseo","root","root"); 
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println("Server mySQL non avviato ");
            System.err.println(ex.getMessage());
        }

        return conn;
    }
}