package servermuseo;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;

class Server {
    
    private HttpServer server = null;
    //funzione di creazione del server con tutte gli handler necessari al funzionamento
    public boolean open(String localhost, int port) throws IOException {
        server = HttpServer.create(new InetSocketAddress(localhost, port), 0);
        server.createContext("/testConnessione", new tryConnect());
        server.createContext("/getComment", new getComment());
        server.createContext("/comment", new comment());
        server.createContext("/getAdminKey", new getAdminKey());
        server.createContext("/getClientKey", new getClientKey());
        server.createContext("/publicKey", new publicKey());
        server.createContext("/prenota", new prenota());
        server.createContext("/payServer", new payServer());
        server.createContext("/getOrari", new getOrari());
        server.createContext("/getPrezzi", new getPrezzi());
        server.createContext("/loginClient", new loginClient());
        server.createContext("/loginAdmin", new loginAdmin());
        server.createContext("/getPrenData", new getPrenData());
        server.createContext("/getVisitData", new getVisitData());
        server.createContext("/getGradData", new getGradData());
        server.createContext("/getUser", new getUser());
        server.createContext("/getForUp", new getForUp());
        server.createContext("/updateData", new updateData());
        server.createContext("/nuovoAdmin", new nuovoAdmin());
        server.createContext("/rimuoviAdmin", new rimuoviAdmin());
        server.createContext("/twitte", new twitter());
        server.createContext("/tokenServer", new tokenServer());
        server.createContext("/validateTokenServer", new validateTokenServer());
        server.createContext("/validatePayServer", new validatePayServer());
        server.setExecutor(null);
        server.start();        
    return true;
    }    
}
