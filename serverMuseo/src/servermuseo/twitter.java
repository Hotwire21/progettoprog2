package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class twitter implements HttpHandler {
    
    //handler per la prova della connessione
    public void handle(HttpExchange he) throws IOException {
        Map<String, String> params = queryToMap(he.getRequestURI().getQuery());
        conf.oauth=params.get("oauth_verifier");
        Headers map = he.getResponseHeaders();
        String redirect = "http://mume.altervista.org/appJava.php";
        map.add("Location", redirect);
        he.sendResponseHeaders(301, -1);
        he.close();
        
    }
    
    public Map<String, String> queryToMap(String query) {
    Map<String, String> result = new HashMap<>();
    for (String param : query.split("&")) {
        String[] entry = param.split("=");
        if (entry.length > 1) {
            result.put(entry[0], entry[1]);
        }else{
            result.put(entry[0], "");
        }
    }
    return result;
}
}    

