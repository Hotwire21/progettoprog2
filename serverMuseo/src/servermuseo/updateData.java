package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import twitter4j.JSONException;
import twitter4j.TwitterException;

public class updateData implements HttpHandler {
    
    //handler per modifica dei dati
    public void handle(HttpExchange he) throws IOException {    
        try {
            System.out.println(conf.ANSI_GREEN+"Richiesta di aggiornamento delle tabelle orari e prezzi!"+conf.ANSI_RESET);
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey adminKey= ac.getPublic(conf.dir+"adminKey");            
            
            // ricevo i dati dal client
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            // decifra la stringaricevuta dal client            
            String str=ac.decryptText(new String(data), privKey);
            //System.out.println("Messaggio ricevuto = " + new String(data));
            // passa i dati decifrati alla classe che eseguirà le operazione di modifica sul database
            Query qr = new Query();
            String r= qr.updateData(str);
            // cifra la stringa(JSON) da inviare al client            
            String res=ac.encryptText(r, adminKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            //Dato di risposta
            os.write(output);           
            os.close();
            
        } catch (TwitterException | JSONException ex) {
            Logger.getLogger(updateData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(updateData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(updateData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(updateData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}

   
    