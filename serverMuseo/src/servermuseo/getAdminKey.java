package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class getAdminKey implements HttpHandler {
     //handler per il salvataggio della chiave pubblica dell'admin
     public void handle(HttpExchange he) throws IOException {
         try {
             Headers requestHeaders = he.getRequestHeaders();
             //acquisisco i dati dal client
             int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
             InputStream is = he.getRequestBody();
             byte[] data = new byte[contentLength];
             is.read(data);
             //System.out.println("Messaggio ricevuto = " + new String(data));
             System.out.println("Ricezione chiave pubblica admin!");
             //scrivo lo chiave sul file
             AsymmetricCryptography ac = new AsymmetricCryptography();
             File f =new File(conf.dir+"adminKey");
             ac.writeToFile(f, data);             
             byte[] output = "ok".getBytes();
             he.sendResponseHeaders(200, output.length);
             OutputStream os = he.getResponseBody();
             os.write(output);
             os.close();
         } catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(getAdminKey.class.getName()).log(Level.SEVERE, null, ex);
         } catch (NoSuchPaddingException ex) {
             Logger.getLogger(getAdminKey.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IllegalBlockSizeException ex) {
             Logger.getLogger(getAdminKey.class.getName()).log(Level.SEVERE, null, ex);
         } catch (BadPaddingException ex) {
             Logger.getLogger(getAdminKey.class.getName()).log(Level.SEVERE, null, ex);
         }
     }  
}
