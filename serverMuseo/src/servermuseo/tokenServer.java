package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONObject;

public class tokenServer implements HttpHandler {
    
    //handler per l'invio della chiave pubblica del server
    public void handle(HttpExchange he) throws IOException {
        try {
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"adminKey");            
            
            JSONObject returnJson = new JSONObject();
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            String str=ac.decryptText(new String(data), privKey);
            //System.out.println("Messaggio ricevuto = " + str);
            System.out.println(conf.ANSI_GREEN+"Richiesta del token !"+conf.ANSI_RESET);
            //la query restituirà gli orari presenti nel database
            FunctionSuper fs = new FunctionSuper();
            fs.tokenServer(str);
            
            //cifra il dato di risposta
            String res=ac.encryptText(returnJson.toString(), clientKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            os.write(output);
            os.close();      
            
        } catch (SQLException ex) {
            Logger.getLogger(getOrari.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(getOrari.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(getOrari.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(getOrari.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
    

