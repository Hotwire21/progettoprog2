package servermuseo;

import com.dropbox.core.DbxException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import twitter4j.JSONException;
import twitter4j.JSONObject;

public class prenota implements HttpHandler {    
    //handler per il salvataggio delle prenotazioni
    public void handle(HttpExchange he) throws IOException, FileNotFoundException {        
        try {       
            System.out.println(conf.ANSI_BLUE+"Richiesta di prenotazione!"+conf.ANSI_RESET);
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");            
            
            // ricevo i dati dal client
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            // decifra la stringaricevuta dal client            
            String str=ac.decryptText(new String(data), privKey);
            
            // passa i dati decifrati alla classe che eseguirà le operazione di prenotazione
            FunctionSuper fs = new FunctionSuper();
            String a = fs.prenota(str);
            
            //controllo risposta restituita dalla funzione
            JSONObject a1 = new JSONObject(a);
            if("false".equals((String) a1.get("exec"))){                
                // cifra la stringa(JSON) da inviare al client  
                String res=ac.encryptText("{\"err\":\"errore\"}", clientKey);
                byte[] output = res.getBytes();
                he.sendResponseHeaders(200, output.length);
                //Dato di risposta
                OutputStream os = he.getResponseBody();
                //Dato di risposta
                os.write(output);
                os.close();                                 
                System.out.println(conf.ANSI_RED+"---> Richesta di prenotazione non completata!"+conf.ANSI_RESET);
            }else{
                String b1=(String) a1.get("id");
                String b2=(String) a1.get("data");
                // funzione che salva il pdf che ha come nome la data della prenotazione, sulla cartella dropbox che ha come nome l'id dell'utente
                String b = fs.addPDF(b1, b2);
                
                // cifra la stringa(JSON) da inviare al client                
                String res=ac.encryptText(b, clientKey);
                byte[] output = res.getBytes();           
                he.sendResponseHeaders(200, output.length);
                OutputStream os = he.getResponseBody();
                os.write(output);
                os.close();
                System.out.println(conf.ANSI_BLUE+"---> Richesta di prenotazione completata con successo!"+conf.ANSI_RESET);
            }
        } catch (JSONException ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DbxException ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(prenota.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
