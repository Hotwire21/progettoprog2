package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;

public class publicKey implements HttpHandler {
    
    //handler per l'invio della chiave pubblica del server
    public void handle(HttpExchange he) throws IOException {
        try {
            Headers requestHeaders = he.getRequestHeaders();
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            
            //lettura della chiave dal file
            AsymmetricCryptography ac = new AsymmetricCryptography();
            File f= new File(conf.dir+"publicKey");
            byte[] output =ac.getFileInBytes(f);
            he.sendResponseHeaders(200, output.length);
            
            //invio della risposta
            System.out.println("Invio chiave pubblica");
            OutputStream os = he.getResponseBody();
            os.write(output);
            os.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(publicKey.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(publicKey.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    

