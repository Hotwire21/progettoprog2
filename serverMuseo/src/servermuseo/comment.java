package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import twitter4j.JSONException;
import twitter4j.TwitterException;

public class comment implements HttpHandler {

    //handler per il salvataggio del commento
    public void handle(HttpExchange he) throws IOException {    
        try {
            System.out.println(conf.ANSI_BLUE+"Richiesta inserimento commento"+conf.ANSI_RESET);
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");            
            
            // ricevo i dati dal client
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            // decifra la stringaricevuta dal client            
            String str=ac.decryptText(new String(data), privKey);
            //System.out.println("Messaggio ricevuto = " + new String(data));
            // passa i dati decifrati alla classe che eseguirà la query
            Query qr = new Query();
            qr.setComment(str);
            
            // cifra la stringa(JSON) da inviare al client            
            String res=ac.encryptText("{\"res\":\"ok\"}", clientKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            os.write(output);           
            os.close();
            
        } catch (TwitterException | JSONException ex) {
            Logger.getLogger(comment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(comment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(comment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(comment.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

   
    