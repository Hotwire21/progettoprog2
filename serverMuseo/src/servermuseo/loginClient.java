package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import twitter4j.JSONException;
import twitter4j.TwitterException;

public class loginClient implements HttpHandler {

    public void handle(HttpExchange he) throws IOException {    
        try {
            
            //handler per il login dei clienti
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");            
            
            // ricevo i dati dal client
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            // decifra la stringaricevuta dal client            
            String str=ac.decryptText(new String(data), privKey);
            //System.out.println("Messaggio ricevuto = " + new String(data));
            // passa i dati decifrati alla classe che eseguirà le operazione di login tramite twitter
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(str);
            String pin =(String) json.get("pin");
            FunctionSuper fs = new FunctionSuper();
           
            String r=null;
            if ("false".equals(pin)){
                System.out.println(conf.ANSI_BLUE+"Richiesta di login da parte di un utente!"+conf.ANSI_RESET);
                r=fs.getLink();
            }else{
                System.out.println(conf.ANSI_BLUE+"---> Ricezione del pin di twitter!"+conf.ANSI_RESET);
                r=fs.getTok(conf.oauth, 0);
            } 
            // cifra la stringa(JSON) da inviare al client            
            String res=ac.encryptText(r, clientKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            //Dato di risposta
            os.write(output);           
            os.close();
                  
        } catch (TwitterException | JSONException ex) {
            Logger.getLogger(loginClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(loginClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(loginClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(loginClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}

   
    