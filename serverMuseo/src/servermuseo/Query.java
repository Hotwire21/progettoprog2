package servermuseo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import twitter4j.JSONException;

public class Query extends DataBase_Connect {
    
    //Query di selezione ultimi 10 commenti in memoria
    public JSONObject getComment() throws SQLException {
        Connection conn;
        String id = null;
        String text = null;
        String feel = null;       
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        Statement st = conn.createStatement();
        String query = "SELECT uniqueID, text, feel FROM commenti order by id desc LIMIT 10";        
        ResultSet rs = st.executeQuery(query);
        //inserimento risulatati della query all'interno di un array dentro un ogetto json
        JSONArray array = new JSONArray();
        while (rs.next()) {                      
            JSONObject item = new JSONObject();
            id=rs.getString("uniqueID");
            text=rs.getString("text");
            feel=rs.getString("feel");                      
            item.put("id", id);
            item.put("text", text);
            item.put("feel", feel);
            array.add(item);                                               
        }                
        returnJson.put("commenti", array);
        st.close();
        return returnJson;                   
    } 
    
    //Query di inserimento commento nel database
    public void setComment(String str) throws JSONException, SQLException {
        Connection conn;
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        twitter4j.JSONObject json=new twitter4j.JSONObject(str);           
        String id = (String) json.get("id");
        String feel = (String) json.get("feel");
        String testo = (String) json.get("testo");
        testo= testo.replace("'", "");
        String query = "INSERT INTO commenti ( uniqueID, text, feel) VALUES ('" + id + "','" + testo + "','" + feel + "')";
        conn.createStatement().executeUpdate(query);
        System.out.println(conf.ANSI_BLUE+"---> E' stato aggiunto un nuovo commento!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET); 
       
    }
    //Query di selezione dell'utente che si vuole loggare se l'utente non esiste lo si crea
    public void setUser(String id, String nome, String key1, String key2, String idtw) {
        Connection conn;
        try {
            DataBase_Connect db = new DataBase_Connect();
            conn = db.Connessione();
            //Query
            String query = "SELECT uniqueID FROM utenti WHERE uniqueID='"+id+"'";            
            Statement st = conn.createStatement(); 
            ResultSet rs = st.executeQuery(query);
            if (rs.next()==true) {
               System.out.println(conf.ANSI_BLUE+"---> L'utente è stato trovato"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
            } else {                
                query = "INSERT INTO utenti ( uniqueID, nome, acc_key, sec_key, permesso, id_tw) VALUES ('" + id + "','" + nome + "','" + key1 + "','"+key2+"','2','"+idtw+"')";
                conn.createStatement().executeUpdate(query);
                System.out.println(conf.ANSI_BLUE+"---> E' stato creato un nuovo utente!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET); 
            }                        
            st.close();            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    //Query di selezione dell'admin che si vuole loggare
    public int setAdmin(String id) {
        Connection conn;
        int a = 0;
        try {
            DataBase_Connect db = new DataBase_Connect();
            conn = db.Connessione();
            String query = "SELECT uniqueID FROM utenti WHERE uniqueID='"+id+"'AND permesso=1";            
            Statement st = conn.createStatement(); 
            ResultSet rs = st.executeQuery(query);
            if (rs.next()==true) {
                a=0;
                System.out.println(conf.ANSI_GREEN+"---> L'admin è stato trovato!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);  
            } else {
                a=1;                
                System.out.println(conf.ANSI_RED+"--->Serve il permesso di admin!   X"+conf.ANSI_RESET);
            }                        
            st.close();            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return a;
    }
    //Query di selezione per il controllo delle prenotazioni con successivo inserimento se non sono presenti altre con conteggio
    //delle prenotazioni inserite che una volta raggiunte le 5 provvede ad applicare un buonon sconto sul totale
    public int setPre(String id, String nome, String bambini, String adulti, String ridotti, int totale, String giorno) throws SQLException {  
        Connection conn;
        int a = 0;
        Timestamp  date = new Timestamp(System.currentTimeMillis());
        try{
            DataBase_Connect db = new DataBase_Connect();
            conn = db.Connessione();
            String query = "SELECT id_utente FROM prenotazioni WHERE giorno='"+giorno+"'";            
            Statement st = conn.createStatement(); 
            ResultSet rs = st.executeQuery(query);
            if (rs.next()==true) {                    
               System.out.println(conf.ANSI_RED+"---> Prenotazione esistente!   X"+conf.ANSI_RESET);
            } else {    
                
                String query1 = "SELECT count(id_utente) FROM prenotazioni WHERE id_utente='"+id+"'";            
                Statement st1 = conn.createStatement(); 
                ResultSet rs1 = st1.executeQuery(query1);
                rs1.next();
                int rowcount = rs1.getInt(1);                    
                int premio=rowcount%5;
                if (rowcount==0 || premio!=0){
                    a=1;
                    String query2 = "INSERT INTO prenotazioni ( id_utente, nome, num_bambini, num_adulti, num_ridotto, prezzo_tot, giorno, data_pre) VALUES ('" + id + "','" + nome + "','" + bambini + "','"+adulti+"','"+ridotti+"',"+totale+",'"+giorno+"','"+date+"')";
                    conn.createStatement().executeUpdate(query2);
                    System.out.println(conf.ANSI_BLUE+"---> Prenotazione effettuata con successo!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
                }else if (premio==0){
                    a=2;
                    int tot=(10*totale)/100;
                    totale=totale-tot;
                    String query2 = "INSERT INTO prenotazioni ( id_utente, nome, num_bambini, num_adulti, num_ridotto, prezzo_tot, giorno, data_pre) VALUES ('" + id + "','" + nome + "','" + bambini + "','"+adulti+"','"+ridotti+"',"+totale+",'"+giorno+"','"+date+"')";
                    conn.createStatement().executeUpdate(query2);
                    System.out.println(conf.ANSI_BLUE+"---> E' stata effettuata una prenotazione con il bonus!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
                }
            }                                       
        } catch (SQLException ex) {
            System.out.println(ex);
        }    
        return a;
    }
    
    //Query di selezione degli orari
    public JSONObject getTime() throws SQLException {
        Connection conn;
        String giorno = null;
        String openM = null;
        String closeM = null;
        String openP = null;
        String closeP = null;

        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT * FROM orari";        
        ResultSet rs = st.executeQuery(query);
        //inserimento risulatati all'interno di un array dentro un ogetto json
        JSONArray array = new JSONArray();
        while (rs.next()) {                      
            JSONObject item = new JSONObject();
            giorno=rs.getString("giorno");
            openM=rs.getString("openM");
            closeM=rs.getString("closeM");
            openP=rs.getString("openP");
            closeP=rs.getString("closeP");            
            item.put("giorno", giorno);
            item.put("mattina", openM+":00-"+closeM+":00");
            item.put("pomeriggio", openP+":00-"+closeP+":00");
            array.add(item);                                               
        }                
        returnJson.put("giorni", array);
        st.close();
        return returnJson;                   
    }         
    
    //Query di selezione dei prezzi
    public JSONObject getPrice() throws SQLException {
        String nome = null;
        int prezzo = 0;
        Connection conn;        
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();        
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT * FROM prezzi";  
        ResultSet rs = st.executeQuery(query);
        JSONArray array = new JSONArray();
        while (rs.next()) { 
            JSONObject item = new JSONObject();
            nome=rs.getString("nome");
            prezzo=rs.getInt("prezzo");
            item.put("nome", nome);
            item.put("prezzo", prezzo);
            array.add(item);
        }
        returnJson.put("prezzi", array);
        st.close();
        return returnJson;                   
    }
    
    //Query di selezione delle prenotazioni per la stampa dei grafici
    public JSONObject getPrenData() throws SQLException {

        Connection conn;        
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();        
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT giorno FROM prenotazioni";  
        ResultSet rs = st.executeQuery(query);
        JSONArray array = new JSONArray();
        while (rs.next()) {            
            array.add(rs.getString("giorno"));
        }
        returnJson.put("giorni", array);
        st.close();
        return returnJson;                   
    }
    //Query di selezione delle persone per la stampa dei grafici
    public JSONObject getVisitData() throws SQLException {
        Connection conn;        
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();        
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT num_bambini, num_adulti, num_ridotto FROM prenotazioni";  
        ResultSet rs = st.executeQuery(query);
        JSONArray array = new JSONArray();
        while (rs.next()) {
            JSONObject item = new JSONObject();
            item.put("num_bambini", rs.getString("num_bambini"));
            item.put("num_adulti", rs.getString("num_adulti"));
            item.put("num_ridotto", rs.getString("num_ridotto"));
            array.add(item);
        }
        returnJson.put("visit", array);
        st.close();
        return returnJson;                   
    }
    //Query di selezione delle persone per la stampa dei grafici
    public JSONObject getGradData() throws SQLException {
        Connection conn;        
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();        
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT feel FROM commenti";  
        ResultSet rs = st.executeQuery(query);
        JSONArray array = new JSONArray();
        while (rs.next()) {
            array.add(rs.getString("feel"));
        }
        returnJson.put("feels", array);
        st.close();
        return returnJson;                   
    }
    
    public JSONObject getPriceTime() throws SQLException {
        String giorno = null;
        String openM = null;
        String closeM = null;
        String openP = null;
        String closeP = null;
        String nome = null;
        int prezzo = 0;

        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();
        Connection conn;
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT * FROM orari";        
        ResultSet rs = st.executeQuery(query);
        //inserimento risulatati all'interno di un array dentro un ogetto json
        JSONArray array = new JSONArray();
        while (rs.next()) {                      
            JSONObject item = new JSONObject();
            giorno=rs.getString("giorno");
            openM=rs.getString("openM");
            closeM=rs.getString("closeM");
            openP=rs.getString("openP");
            closeP=rs.getString("closeP");            
            item.put("giorno", giorno);
            item.put("mattina", openM+"-"+closeM);
            item.put("pomeriggio", openP+"-"+closeP);
            array.add(item);                                               
        }                
        returnJson.put("giorni", array);
        JSONArray array1 = new JSONArray();
        String query1 = "SELECT * FROM prezzi";  
        ResultSet rs1 = st.executeQuery(query1);
        while (rs1.next()) { 
            JSONObject item1 = new JSONObject();
            nome=rs1.getString("nome");
            prezzo=rs1.getInt("prezzo");
            item1.put("nome", nome);
            item1.put("prezzo", prezzo);
            array1.add(item1);
        }
        returnJson.put("prezzi", array1);
        st.close();        
        return returnJson;                   
    }      
    
    public JSONObject getUser() throws SQLException {
        Connection conn;        
        JSONObject returnJson = new JSONObject();        
        DataBase_Connect db = new DataBase_Connect();        
        conn = db.Connessione();
        Statement st = conn.createStatement(); 
        String query = "SELECT uniqueID, nome, permesso FROM utenti";  
        ResultSet rs = st.executeQuery(query);
        JSONArray array = new JSONArray();
        while (rs.next()) {
            JSONObject item = new JSONObject();
            item.put("id", rs.getString("uniqueID"));
            item.put("nome", rs.getString("nome"));
            item.put("permesso", rs.getString("permesso"));
            array.add(item);
        }
        returnJson.put("utenti", array);
        st.close();
        return returnJson;                   
    }
 
    //Query di modifica dati nel database
    public String updateData(String str) throws JSONException, SQLException, ParseException {
        Connection conn;
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(str);
        JSONArray array = (JSONArray)json.get("giorni");
        JSONArray array1 = (JSONArray)json.get("prezzi");
        for(int i=0;i<array.size();i++){
            JSONObject name = (JSONObject)array.get(i);
            if((name.get("giorno").equals("Lunedì"))){
                String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
            if((name.get("giorno").equals("Martedì"))){
                 String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
            if((name.get("giorno").equals("Mercoledì"))){
                 String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
            if((name.get("giorno").equals("Giovedì"))){ 
                String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }     
            if((name.get("giorno").equals("Venerdì"))){
                String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query); 
            }
            if((name.get("giorno").equals("Sabato"))){
                String query = "UPDATE orari SET openM='"+String.valueOf(name.get("openM"))+"', closeM='"+String.valueOf(name.get("closeM"))+"', openP='"+String.valueOf(name.get("openP"))+"', closeP='"+String.valueOf(name.get("closeP"))+"' WHERE giorno='"+String.valueOf(name.get("giorno"))+"'"; 
                conn.createStatement().executeUpdate(query); 
            }
        }    
        for(int i=0;i<array1.size();i++){
            JSONObject name = (JSONObject)array1.get(i);
            if((name.get("nome").equals("Adulto"))){
                String query = "UPDATE prezzi SET prezzo='"+String.valueOf(name.get("prezzo"))+"' WHERE nome ='"+String.valueOf(name.get("nome"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
            if((name.get("nome").equals("Bambino"))){
                String query = "UPDATE prezzi SET prezzo='"+String.valueOf(name.get("prezzo"))+"' WHERE nome ='"+String.valueOf(name.get("nome"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
            if((name.get("nome").equals("Ridotto"))){
                String query = "UPDATE prezzi SET prezzo='"+String.valueOf(name.get("prezzo"))+"' WHERE nome ='"+String.valueOf(name.get("nome"))+"'"; 
                conn.createStatement().executeUpdate(query);
            }
        }    
        System.out.println(conf.ANSI_GREEN+"--->Dati aggiornati correttamente!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
      return "{\"ok\":1}"; 
    }
    
    public String nuovoAdmin (String str) throws JSONException, SQLException, ParseException {
        Connection conn;
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(str);
        String id = (String)json.get("id");
        String qr = "UPDATE utenti SET permesso=1 WHERE uniqueID='"+ id +"'";
        System.out.println(conf.ANSI_GREEN+"---> E' stato inserito un nuovo admin!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
        conn.createStatement().executeUpdate(qr);
        return "{\"ok\":1}"; 
    }
    
    public String rimuoviAdmin (String str) throws JSONException, SQLException, ParseException {
        Connection conn;
        DataBase_Connect db = new DataBase_Connect();
        conn = db.Connessione();
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(str);
        String id = (String)json.get("id");
        String qr = "UPDATE utenti SET permesso=2 WHERE uniqueID='"+ id +"'";
        System.out.println(conf.ANSI_GREEN+"---> E' stato rimosso un admin!"+conf.ANSI_PURPLE+" ®"+conf.ANSI_RESET);
        conn.createStatement().executeUpdate(qr);
        return "{\"ok\":2}"; 
        
    }
    
  
}