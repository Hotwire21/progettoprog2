package servermuseo;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class ServerMuseo {    
    public static void main(String args[]) throws IOException {        
        //Creazione chiavi di crittografia pubblica e privata        
        GenerateKeys gk;
        try {
            gk = new GenerateKeys(1024);
            gk.createKeys();
            gk.writeToFile(conf.dir+"publicKey", gk.getPublicKey().getEncoded());
            gk.writeToFile(conf.dir+"privateKey", gk.getPrivateKey().getEncoded());
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //inizializzazione server
        Server server = new Server();
        if (server.open(conf.server, Integer.valueOf(conf.port)) == true) {
            System.out.println("Server avviato sulla porta " + conf.port);
        } else {
            System.out.println("Errore server non avviato");
        }
    }
    
}
