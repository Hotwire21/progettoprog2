package servermuseo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class tryConnect implements HttpHandler {
    
    //handler per la prova della connessione
    public void handle(HttpExchange he) throws IOException {
        Headers requestHeaders = he.getRequestHeaders();
        int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
        InputStream is = he.getRequestBody();
        byte[] data = new byte[contentLength];
        is.read(data);              
        Headers responseHeaders = he.getResponseHeaders();
        
        byte[] output = "".getBytes();  
        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, output.length);
        OutputStream os = he.getResponseBody();
        os.write(output);
        os.flush();
        os.close();
        he.close();
    }
}    

