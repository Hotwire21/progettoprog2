package servermuseo;

import java.io.FileOutputStream;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import java.io.FileNotFoundException;

public class CreatePDF {
    //istanze variabili con definizione stile
    private static String FILE = conf.dir+"pdf.pdf";
    private static Font titolo = new Font(Font.FontFamily.TIMES_ROMAN, 24,Font.BOLD);
    private static Font sottotitolo = new Font(Font.FontFamily.TIMES_ROMAN, 16,Font.BOLD);
    private static Font descrizione = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.NORMAL);
    private static Font pedice = new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.ITALIC);    

    public void create( String nome, String bambini, String adulti, String ridotti, int totale, String data) {
        //--MAIN-- aggiunge tutti i capitoli al file pdf
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addTitle(document);
            addContent(document, nome);
            createTable(document, bambini, adulti, ridotti, totale, data);
            addFooter(document);
            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    //aggiunta titolo e sottotitolo
    private static void addTitle(Document document)throws DocumentException{        
        Paragraph preface = new Paragraph();                  
        preface.add(new Paragraph("MUSEO DI MESSINA", titolo));        
        addEmptyLine(preface, 1); 
        DottedLineSeparator dottedline = new DottedLineSeparator();
        dottedline.setLineColor(BaseColor.LIGHT_GRAY);
        dottedline.setGap(2f);
        preface.add(dottedline);
        addEmptyLine(preface, 2); 
        preface.add(new Paragraph("La tua prenotazione", sottotitolo));        
        addEmptyLine(preface, 1); 
        dottedline.setLineColor(BaseColor.LIGHT_GRAY);
        dottedline.setGap(2f);
        preface.add(dottedline);        
        document.add(preface);      
    }
    
    //aggiunta riepilogo
    private static void addContent(Document document, String nome) throws DocumentException {
        Paragraph preface = new Paragraph();        
        addEmptyLine(preface, 2);        
        preface.add(new Paragraph("Gentile "+nome+" la sua prenotazione è stata confermata.", descrizione));        
        addEmptyLine(preface, 1);         
        preface.add(new Paragraph("La informiamo che dovrà recarsi presso il museo con il seguente documento per effettuare il pagamento,", descrizione));
        preface.add(new Paragraph("una volta concluso avrà pieno accesso alla totalità dei nostri servizi.", descrizione));        
        addEmptyLine(preface, 1);
        DottedLineSeparator dottedline = new DottedLineSeparator();
        dottedline.setLineColor(BaseColor.LIGHT_GRAY);
        dottedline.setGap(2f);
        preface.add(dottedline);
        addEmptyLine(preface, 2);
        document.add(preface);        
    }
//aggiunta tabella con i parametri della prenotazione
    private static void createTable(Document document, String bambini, String adulti, String ridotti, int totale, String data) throws DocumentException{
        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("Riepilogo prenotazione", sottotitolo));
        addEmptyLine(preface, 2);        
        preface.add(new Paragraph("Giorno : "+data, descrizione));  
        addEmptyLine(preface, 2);
        document.add(preface);
        PdfPTable table = new PdfPTable(4);
        table.addCell("Bambini");
        table.addCell("Adulti");
        table.addCell("Ridotti");
        table.addCell("Totale");
        table.addCell("n° "+bambini);
        table.addCell("n° "+adulti);
        table.addCell("n° "+ridotti);
        table.addCell("€ "+totale);
        document.add(table);
    }
    //aggiunta pedice con descizione
     private static void addFooter(Document document) throws DocumentException { 
        Paragraph preface = new Paragraph();        
        addEmptyLine(preface, 8);        
        preface.add(new Paragraph("Il seguente documento non è da considerarsi come scontrino fiscale. La seguente copia ha il solo scopo di attestare l'avvenuta prenotazione. Il museo non si assume nessun rischio per quanto riguarda lo smarrimento di essa. Ci riserviamo il diritto di apportare, senza alcun preavviso, tutte le modifiche alla presente Informativa sulla Privacy che riterremo opportune o che saranno rese obbligatorie per legge. Il presente documento rispetta le norme di privacy policy, aggiornato con il Regolamento UE (GDPR) 2016/679 relativo al trattamento dei dati personali, le modalità di trattamento dei dati raccolti da un sito internet durante la navigazione da parte dell'utente.", pedice));        
        addEmptyLine(preface, 1);                 
        document.add(preface);
    }
     //aggiunta righe vuote per la formattazione
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
