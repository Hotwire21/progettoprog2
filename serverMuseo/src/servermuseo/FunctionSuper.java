package servermuseo;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.SQLException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;


public class FunctionSuper {
    
    public String getLink() throws TwitterException, IOException{
    String url=null;
    try {
        conf.twitter= new TwitterFactory().getInstance();
        conf.twitter.setOAuthConsumer(conf.consumer_key, conf.consumer_sec);
        conf.requestToken = conf.twitter.getOAuthRequestToken();
        url=conf.requestToken.getAuthorizationURL();
    } catch (TwitterException ex) {
        throw new RuntimeException(ex);
    }
    return "{\"url\":\""+url+"\"}";
    }
    
    public String getTok(String pin, int permesso) throws TwitterException, IOException, JSONException{
    String res=null;
    if (!"".equals(pin)){
        try {
            twitter4j.auth.AccessToken accessToken = conf.twitter.getOAuthAccessToken(conf.requestToken, pin);
            conf.acTK= accessToken.getToken();
            conf.scTK=accessToken.getTokenSecret();
            res = authTwit(conf.acTK, conf.scTK, permesso);
        }catch (TwitterException ex){
            throw new RuntimeException(ex);
        }
    }else{
        res="{\"res\":\"false\"}";
    }  
    return res;
    }
    
     //fuzione di autenticazione con l'API di twitter
     public String authTwit(String acTK, String scTK, int i) throws TwitterException, JSONException{                             

        //oggetto di configurazione contente le chiavi dell'applicativo di twitter
        ConfigurationBuilder cb = new ConfigurationBuilder();        
        cb.setDebugEnabled(true);
        cb.setOAuthConsumerKey(conf.consumer_key);
        cb.setOAuthConsumerSecret(conf.consumer_sec);
        cb.setOAuthAccessToken(acTK);
        cb.setOAuthAccessTokenSecret(scTK);
        
        //istanza classe twitter riutilizzabile
        TwitterFactory tf = new TwitterFactory(cb.build());
        //istanza per un utente
        Twitter twitter = tf.getInstance();
        //prendo dati dall'utente
        //String tweet="Prova dalla mia app";
        //Status status = twitter.updateStatus(tweet);        
        User user = twitter.showUser(twitter.getId());
        long twid=user.getId();
        String idtw = String.valueOf(twid);
        String foto = user.getBiggerProfileImageURL();
        String us = user.getName();
        String id = user.getScreenName();
        String res="";
        int a=0;
        //query per controllo utente o inserimento se non ancora registrato
        if (i==0){
            Query qr = new Query();
            qr.setUser(id, us, acTK, scTK, idtw);
            res="{\"foto\":\""+foto+"\", \"nome\":\""+us+"\", \"id\":\""+id+"\",\"idtw\":"+idtw+"}";
        }else if(i==1){
            Query qr = new Query();
            a=qr.setAdmin(id);
            res="{\"res\":"+a+",\"foto\":\""+foto+"\", \"nome\":\""+us+"\", \"id\":\""+id+"\",\"idtw\":"+idtw+"}";
        }
        //System.out.println(Arrays.toString(status.getWithheldInCountries()));
        //System.out.println(status.getText());
        //System.out.println(status.getUser());        
       /*        
        JSONObject json = new JSONObject(status.getUser());              
        String name=json.getString("name");
        String screenName=json.getString("screenName");
        String fotoProfilo=json.getString("profileImageUrl");
        System.out.println("Dati Presi da Twitter");        
        System.out.println(user.getProfileImageURL());
        */
       
        conf.requestToken=null;
        conf.oauth=null;
        return res;           
    }
     // fuzione che salva nel database la prenotazione creando il pdf e salvandolo su dropbox
    public String prenota(String data) throws JSONException, SQLException{
        JSONObject json=new JSONObject(data);
        String id = (String) json.get("id");
        String nome = (String) json.get("nome");
        String bambini = (String) json.get("bambini");
        String adulti = (String) json.get("adulti");
        String ridotti = (String) json.get("ridotti");
        int totale = (int) json.get("totale");
        String giorno = (String) json.get("data");                                             
        int res;
        //controllo se i dati siano congruenti con le prenotazioni
        if(totale==0){
            res=0;
        }else{
            Query qr = new Query();
            res=qr.setPre(id, nome, bambini, adulti, ridotti, totale, giorno);
        }     
        String ret="";
        //controllo risposta
        switch (res) {
            //se 0 allora la prenotazione contiene errori
            case 0:
                ret="{\"id\":\""+id+"\",\"data\":\""+giorno+"\",\"exec\":\"false\"}";
                break;
            //se 1 allora la prenotazione può essere effettuata
            case 1:
                {
                    ret="{\"id\":\""+id+"\",\"data\":\""+giorno+"\",\"exec\":\"true\"}";
                    CreatePDF pdf = new CreatePDF();
                    pdf.create(nome, bambini, adulti, ridotti, totale, giorno);
                    System.out.println(conf.ANSI_BLUE+"---> Creazione pdf !"+conf.ANSI_RESET);
                    break;
                }  
            //Se 2 allota verrà effettuato uno sconto sul totale
            case 2:
                {
                    int tot=(10*totale)/100;
                    totale=totale-tot;
                    ret="{\"id\":\""+id+"\",\"data\":\""+giorno+"\",\"exec\":\"true\"}";
                    CreatePDF pdf = new CreatePDF();
                    pdf.create(nome, bambini, adulti, ridotti, totale, giorno);
                    System.out.println(conf.ANSI_BLUE+"---> Creazione pdf !"+conf.ANSI_RESET);
                    break;
                }
            default:
            break;
         }
        return ret;    
    } 
    
    //carica il pdf sulla cartella dropbox con il nome dell'utente sttraverso la chiave dell'API dell'applicazione dropbox
    public String addPDF(String id, String data) throws DbxException, FileNotFoundException, IOException {        
        System.out.println(conf.ANSI_BLUE+"---> Caricamento pdf in dropbox!"+conf.ANSI_RESET);
        String dropbox_tok = conf.dropbox_tok;
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, dropbox_tok);
        // Get current account info
        //FullAccount account = client.users().getCurrentAccount();
        //System.out.println(account.getName().getDisplayName());
        // Get files and folder metadata from Dropbox root directory
        /*
        ListFolderResult result = client.files().listFolder("");
        while (true) {
            for (Metadata metadata : result.getEntries()) {
                System.out.println(metadata.getPathLower());
            }
            if (!result.getHasMore()) {
                break;
            }
            result = client.files().listFolderContinue(result.getCursor());
        }
        */
        try (InputStream in = new FileInputStream(conf.dir+"pdf.pdf")) {
            client.files().uploadBuilder("/"+id+"/prenotazione_del_"+data).uploadAndFinish(in);
        }        
        String link=client.files().getTemporaryLink("/"+id+"/prenotazione_del_"+data).getLink();
        String ret = "{\"link\":\""+link+"\"}";
        File file = new File(conf.dir+"pdf.pdf");
        file.delete();
        return ret;
    }
    
   
    
    public void keyServer() throws MalformedURLException, ProtocolException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
        URL obj1 = new URL("http://localhost:9001/publicKey");            
        HttpURLConnection con1 = (HttpURLConnection) obj1.openConnection();                
        con1.setRequestMethod("GET");
        con1.setRequestProperty("Accept-Language", "en-US");             
        con1.setDoOutput(true);                
        DataOutputStream wr1 = new DataOutputStream(con1.getOutputStream());
        wr1.write("".getBytes());
        wr1.flush();
        wr1.close();
        //ottengo la risposta
        int dim_Response = con1.getContentLength();            
        DataInputStream in = new DataInputStream(con1.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        in.close();
        //scrive la chiave su di un file
        AsymmetricCryptography ac = new AsymmetricCryptography();
        File f =new File(conf.dir+"serverTokensKey");
        ac.writeToFile(f, data);
        
        URL obj2 = new URL("http://localhost:9001/getPublic");            
        HttpURLConnection con2 = (HttpURLConnection) obj2.openConnection();
        con2.setRequestMethod("POST");
        con2.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        File f2 =new File(conf.dir+"publicKey");
        byte[] myKey=ac.getFileInBytes(f2);
        con2.setDoOutput(true);
        DataOutputStream wr2 = new DataOutputStream(con2.getOutputStream());
        wr2.write(myKey);
        wr2.flush();
        wr2.close();                                                
        int dim_Response2 = con2.getContentLength();            
        DataInputStream in2 = new DataInputStream(con2.getInputStream());                                    
        byte[] data2 = new byte[dim_Response2];            
        in2.readFully(data2);
        in2.close();  
    }            
    
    
    public void tokenServer(String str) throws TwitterException, IOException, JSONException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, Exception{
        keyServer();
        AsymmetricCryptography ac = new AsymmetricCryptography();
        URL obj3 = new URL("http://localhost:9001/getTok");            
        HttpURLConnection con3 = (HttpURLConnection) obj3.openConnection();
        con3.setRequestMethod("POST");
        con3.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        PublicKey tokenKey= ac.getPublic(conf.dir+"serverTokensKey");   
        String res=ac.encryptText(str, tokenKey);
        byte[] myStr= res.getBytes();
        con3.setDoOutput(true);
        DataOutputStream wr3 = new DataOutputStream(con3.getOutputStream());
        wr3.write(myStr);
        wr3.flush();
        wr3.close();                                                
        int dim_Response3 = con3.getContentLength();            
        DataInputStream in3 = new DataInputStream(con3.getInputStream());                                    
        byte[] data3 = new byte[dim_Response3];            
        in3.readFully(data3);
        in3.close();  
   
    }
    
    public String validateServer(String str) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, Exception{
        AsymmetricCryptography ac = new AsymmetricCryptography();
        URL obj = new URL("http://localhost:9001/valTok");            
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        PublicKey tokenKey= ac.getPublic(conf.dir+"serverTokensKey");
        PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
        String res=ac.encryptText(str, tokenKey);
        byte[] myStr= res.getBytes();
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(myStr);
        wr.flush();
        wr.close();                                                
        int dim_Response = con.getContentLength();            
        DataInputStream in = new DataInputStream(con.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        String d=ac.decryptText(new String(data), privKey);
        return d;
    }
    
    public void sendCodServer(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, MalformedURLException, ProtocolException, Exception{
        keyServer();
        AsymmetricCryptography ac = new AsymmetricCryptography();
        URL obj = new URL("http://localhost:9001/sendMsg");            
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        PublicKey tokenKey= ac.getPublic(conf.dir+"serverTokensKey");
        PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
        String res=ac.encryptText(str, tokenKey);
        byte[] myStr= res.getBytes();
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(myStr);
        wr.flush();
        wr.close();
        int dim_Response = con.getContentLength();            
        DataInputStream in = new DataInputStream(con.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        ac.decryptText(new String(data), privKey);
    }
    
    public String valCodServer(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, MalformedURLException, ProtocolException, Exception{
        AsymmetricCryptography ac = new AsymmetricCryptography();
        URL obj = new URL("http://localhost:9001/valCod");            
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        PublicKey tokenKey= ac.getPublic(conf.dir+"serverTokensKey");
        PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
        String res=ac.encryptText(str, tokenKey);
        byte[] myStr= res.getBytes();
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(myStr);
        wr.flush();
        wr.close();                                                
        int dim_Response = con.getContentLength();            
        DataInputStream in = new DataInputStream(con.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        String d=ac.decryptText(new String(data), privKey);
        return d;
    }
    
        
}




