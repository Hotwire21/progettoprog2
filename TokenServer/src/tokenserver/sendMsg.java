package tokenserver;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class sendMsg implements HttpHandler {
    public void handle(HttpExchange he) throws IOException {
        try {
            
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            String str=ac.decryptText(new String(data), privKey);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(str);
            String tel = (String)json.get("tel");
            createToken(tel);
            
            System.out.println("Creazione token e invio come messaggio telefonico!");
            //creiamo un metodo di validazione
            String res=ac.encryptText("", clientKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            os.write(output);
            os.close(); 
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(sendMsg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(sendMsg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(sendMsg.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void createToken(String tel) throws NoSuchAlgorithmException, ProtocolException, IOException, MalformedURLException, MessagingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        Date date= new Date();
        SimpleDateFormat sd= new SimpleDateFormat("mm:ss");
        String data= sd.format(date);
        conf.dataPay= data; 
        String pass= data;        
        byte[] hashInBytes = md.digest(pass.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        String cod=sb.toString().substring(5,10);
        sendToken(tel, cod);
    }
    
    
     public void sendToken(String tel, String cod) throws MalformedURLException, ProtocolException, IOException, AddressException, MessagingException {
         /*String myPasscode = "giannibello";
         String myUsername = "giannibello";
         String toPhoneNumber = tel;
         String myMessage = "Il%20codice%20di%20pagamento%20della%20prenotazione%20:%20"+cod;
         String url = "http://cloud.fowiz.com/api/message_http_api.php?username="+myUsername+"&phonenumber="+toPhoneNumber+"&message="+myMessage+"&passcode="+myPasscode;
         URL obj = new URL(url);
         HttpURLConnection con = (HttpURLConnection) obj.openConnection();
         con.setRequestMethod("GET");
         
         BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
         String inputLine;
         StringBuilder response = new StringBuilder();
         while ((inputLine = in.readLine()) != null) {
         response.append(inputLine);
         }
         in.close();*/
        
        // Find your Account Sid and Auth Token at twilio.com/console
        
        final String username = "santuzza.frazzica@gmail.com";
        final String password = "santuzzaFRAZZICA21@";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
            }
          });
        try{
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("mume@comune.messina.it"));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(tel));
            message.setSubject("CODICE VERIFICA PAGAMENTO");
            message.setText("Il codice da inserire per la verifica del pagamento effettuato è "+ cod);
            Transport.send(message);

        } catch (MessagingException e) {
                throw new RuntimeException(e);
        }   
    }
    
}


