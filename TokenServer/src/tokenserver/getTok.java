package tokenserver;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import twitter4j.DirectMessage;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;

public class getTok implements HttpHandler {
    public void handle(HttpExchange he) throws IOException {
        try {
            
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            String str=ac.decryptText(new String(data), privKey);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(str);
            long name = (long)json.get("idtw");
            createToken(5, name);
            System.out.println("Creazione token e invio come direct message sull'account twitter!");
            //creiamo un metodo di validazione
            String res=ac.encryptText("", clientKey);
            byte[] output = res.getBytes();            
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            os.write(output);
            os.close(); 
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(getTok.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(getTok.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(getTok.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void createToken(int length, long name) throws NoSuchAlgorithmException, TwitterException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        Date date= new Date();
        SimpleDateFormat sd= new SimpleDateFormat("mm:ss");
        String data= sd.format(date);
        conf.dataToken= data; 
        String pass= name+data;        
        byte[] hashInBytes = md.digest(pass.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        String a=sb.toString().substring(5,10);
        sendToken(a, name);
    }
    
     public void sendToken(String token, long name) throws NoSuchAlgorithmException, TwitterException {
        TwitterFactory tf= new TwitterFactory();
        Twitter tw= tf.getInstance();
        tw.setOAuthConsumer(conf.consumerKey, conf.consumerSecret);
        AccessToken at= new AccessToken(conf.accessToken, conf.secretToken);
        tw.setOAuthAccessToken(at);
        User user = tw.showUser(tw.getId());
        DirectMessage dM = tw.sendDirectMessage(name, "Il codice di sicurezza da inserire sull'app è: "+token);
    }
    
}
