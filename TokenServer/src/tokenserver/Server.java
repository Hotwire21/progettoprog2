package tokenserver;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;

public class Server {
    private HttpServer server = null;
    //funzione di creazione del server con tutte gli handler necessari al funzionamento
    public boolean open(String localhost, int port) throws IOException {
    server = HttpServer.create(new InetSocketAddress(localhost, port), 0);
    server.createContext("/getTok", new getTok());
    server.createContext("/valTok", new valTok());
    server.createContext("/publicKey", new publicKey());
    server.createContext("/getPublic", new getPublic());
    server.createContext("/sendMsg", new sendMsg());
    server.createContext("/valCod", new valCod());
    server.setExecutor(null);
    server.start();     
    return true;
    }
}
