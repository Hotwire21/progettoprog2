package tokenserver;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class valCod implements HttpHandler {
    public void handle(HttpExchange he) throws IOException {
        try {
            
            AsymmetricCryptography ac = new AsymmetricCryptography();
            Headers requestHeaders = he.getRequestHeaders();    
            PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
            PublicKey clientKey= ac.getPublic(conf.dir+"clientKey");
            int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
            InputStream is = he.getRequestBody();
            byte[] data = new byte[contentLength];
            is.read(data);
            String str=ac.decryptText(new String(data), privKey);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(str);
            String cod= (String)json.get("cod");
            
            String ris=validateToken(cod);
            
            System.out.println("Controllo token!");
            //creiamo un metodo di validazione
            String res=ac.encryptText(ris, clientKey);
            byte[] output = res.getBytes();
            he.sendResponseHeaders(200, output.length);
            OutputStream os = he.getResponseBody();
            os.write(output);
            os.close(); 
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(valCod.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(valCod.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(valCod.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String validateToken(String cod) throws NoSuchAlgorithmException {
        String old=createToken();
        String res="";
        if (cod.equals(old)){
            res="{\"ok\":\"1\"}";
        }else{
            res="{\"ok\":\"2\"}";
        }
        return res;
    }
    
    public String createToken() throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(conf.dataPay.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        String cod=sb.toString().substring(5,10);
        return cod;
    }
    
}
