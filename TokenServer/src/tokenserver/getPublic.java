package tokenserver;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class getPublic implements HttpHandler {
     //handler per il salvataggio della chiave pubblica del client
     public void handle(HttpExchange he) throws IOException {
         try {
             Headers requestHeaders = he.getRequestHeaders();             
             int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));
             InputStream is = he.getRequestBody();
             byte[] data = new byte[contentLength];
             is.read(data);
             //System.out.println("Messaggio ricevuto = " + new String(data));
             System.out.println("Ricezione chiave pubblica!");
            //scrivo lo chiave sul file
             AsymmetricCryptography ac = new AsymmetricCryptography();
             File f =new File(conf.dir+"clientKey");
             ac.writeToFile(f, data);             
             byte[] output = "ok".getBytes();
             he.sendResponseHeaders(200, output.length);
             OutputStream os = he.getResponseBody();
             os.write(output);
             os.close();
         } catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(getPublic.class.getName()).log(Level.SEVERE, null, ex);
         } catch (NoSuchPaddingException ex) {
             Logger.getLogger(getPublic.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IllegalBlockSizeException ex) {
             Logger.getLogger(getPublic.class.getName()).log(Level.SEVERE, null, ex);
         } catch (BadPaddingException ex) {
             Logger.getLogger(getPublic.class.getName()).log(Level.SEVERE, null, ex);
         }
    }  
}
