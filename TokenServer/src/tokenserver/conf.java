package tokenserver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class conf {
    static String dir;
    static String server;
    static String port;
    static String name;
    static String consumerKey;
    static String consumerSecret;
    static String accessToken;
    static String secretToken;
    static String dataToken;
    static String dataPay; 
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    
     // LEGGERE FILE DI CONFIGURAZIONE
    static {
        try {
            JSONParser parser = new JSONParser();
            String filePath = new File("").getAbsolutePath();
            Object obj = parser.parse(new FileReader(filePath+"/src/projectFiles/conf.json"));
            JSONObject jsonObject = (JSONObject) obj;
            name = (String) jsonObject.get("name");
            dir = (String) jsonObject.get("directory");                        
            server = (String) jsonObject.get("server_address");
            port = (String) jsonObject.get("server_port");
            consumerKey = (String) jsonObject.get("consumer_key");
            consumerSecret = (String) jsonObject.get("consumer_sec");
            accessToken= (String) jsonObject.get("access_token");
            secretToken = (String) jsonObject.get("secret_token");
        } catch (IOException | ParseException ex) {
            Logger.getLogger(conf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
