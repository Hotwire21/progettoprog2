package tokenserver;


import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.NoSuchPaddingException;


public class tokenServer {


    public static void main(String[] args) throws IOException, NoSuchPaddingException {
        
        //Creazione chiavi di crittografia pubblica e privata        
        generateKeys gk;
        try {
            gk = new generateKeys(1024);
            gk.createKeys();
            gk.writeToFile(conf.dir+"publicKey", gk.getPublicKey().getEncoded());
            gk.writeToFile(conf.dir+"privateKey", gk.getPrivateKey().getEncoded());
            } catch (NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
                System.err.println(e.getMessage());
            }
        //inizializzazione server
        Server server = new Server();
        if (server.open(conf.server, Integer.valueOf(conf.port)) == true) {
            System.out.println("Server avviato sulla porta " + conf.port);
        } else {
            System.out.println("Errore server non avviato");
        }
    }
    }
    

