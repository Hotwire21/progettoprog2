package clientmuseo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ProtocolException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_Commenti extends javax.swing.JFrame {

    
    public GUI_Commenti(int err) throws IOException, ProtocolException, ParseException, ParseException, ParseException, ParseException, ParseException, ParseException, ParseException, NoSuchAlgorithmException, NoSuchAlgorithmException, NoSuchAlgorithmException, NoSuchAlgorithmException{       
        try {
            //inizializzazione dei componenti della GUI
            initComponents();
            if (err==1){
                saluto1.setText("Messaggio non valido!");
                saluto1.setForeground(Color.RED);
            }
            ButtonGroup bg = new ButtonGroup();
            bg.add(dislike);
            bg.add(like);
            like.setContentAreaFilled(true);
            request req = new request();
            //richiesta per ottenere gli ultimi 10 commenti ed inserirli nella tabella
            JSONObject returnJson = req.getRequest(conf.server, conf.port, "getComment");
            JSONArray array = (JSONArray)returnJson.get("commenti");
            for(int i=0;i<array.size();i++){
                JSONObject name = (JSONObject)array.get(i);
                tabellaCommenti.setValueAt(name.get("id"), i, 0);
                tabellaCommenti.setValueAt(name.get("text"), i, 1);
                tabellaCommenti.setValueAt(name.get("feel"), i, 2);                
                /*JLabel label = new JLabel();
                if(name.get("feel")=="LIKE"){
                    label.setIcon(new ImageIcon("https://www.google.it/imgres?imgurl=https%3A%2F%2Fwww.shareicon.net%2Fdownload%2F2016%2F07%2F09%2F119075_thumb.ico&imgrefurl=https%3A%2F%2Fwww.shareicon.net%2Fthumb-like-icon-like-thumbs-up-like-button-liked-119075&docid=0r0AOCrpWOOhjM&tbnid=pQYwbBtRIOyv1M%3A&vet=10ahUKEwiji6GmwdvcAhUJSJoKHVrRAXMQMwhJKBAwEA..i&w=256&h=256&bih=1099&biw=1920&q=image%20like&ved=0ahUKEwiji6GmwdvcAhUJSJoKHVrRAXMQMwhJKBAwEA&iact=mrc&uact=8"));
                    tabellaCommenti.setValueAt(label, i, 2);
                }else{
                    label.setIcon(new ImageIcon("https://www.google.it/imgres?imgurl=http%3A%2F%2Fwww.myiconfinder.com%2Fuploads%2Ficonsets%2F256-256-61c87da13517972f054915c9e7419d9f-unlike.png&imgrefurl=http%3A%2F%2Fwww.myiconfinder.com%2Ficon%2Funlike-bad-dislike-finger-hand-not-good-thumb-down%2F12615&docid=iAFAsbcwlVzr3M&tbnid=0MbmWg-1Bxg_MM%3A&vet=10ahUKEwiXpp-xwdvcAhUEyKYKHYGnBdIQMwg0KAIwAg..i&w=256&h=256&bih=1099&biw=1920&q=image%20unlike&ved=0ahUKEwiXpp-xwdvcAhUEyKYKHYGnBdIQMwg0KAIwAg&iact=mrc&uact=8"));
                    tabellaCommenti.setValueAt(label, i, 2);
                }*/
            }
            //mostra il testo cliccato all'interno della tabella
            tabellaCommenti.addMouseListener(new java.awt.event.MouseAdapter(){
                public void mouseClicked(java.awt.event.MouseEvent e)
                {
                    int row=tabellaCommenti.rowAtPoint(e.getPoint());
                    int col= tabellaCommenti.columnAtPoint(e.getPoint());
                    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                    JFrame frame = new JFrame("Hai cliccato");
                    TextArea ta= new TextArea(tabellaCommenti.getValueAt(row,col).toString());
                    ta.setEditable(false);
                    frame.add(ta);
                    frame.setSize(400, 100);
                    frame.setVisible(true);
                    frame.setResizable(false);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
                }
            });
            tabellaCommenti.getTableHeader().setReorderingAllowed(false);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        Titolo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabellaCommenti = new javax.swing.JTable();
        saluto1 = new javax.swing.JLabel();
        insertText = new javax.swing.JLabel();
        maxValue = new javax.swing.JLabel();
        dislike = new javax.swing.JRadioButton();
        like = new javax.swing.JRadioButton();
        insert = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        back = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        tabellaCommenti.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Utente", "Commento", "Feeds"
            }
        ));
        tabellaCommenti.setToolTipText("");
        tabellaCommenti.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabellaCommenti.setAutoscrolls(false);
        tabellaCommenti.setEnabled(false);
        tabellaCommenti.setRequestFocusEnabled(false);
        tabellaCommenti.setRowSelectionAllowed(false);
        tabellaCommenti.setShowHorizontalLines(false);
        tabellaCommenti.setShowVerticalLines(false);
        tabellaCommenti.getTableHeader().setResizingAllowed(false);
        tabellaCommenti.getTableHeader().setReorderingAllowed(false);
        tabellaCommenti.setUpdateSelectionOnSort(false);
        tabellaCommenti.setVerifyInputWhenFocusTarget(false);
        tabellaCommenti.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tabellaCommentiMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(tabellaCommenti);
        if (tabellaCommenti.getColumnModel().getColumnCount() > 0) {
            tabellaCommenti.getColumnModel().getColumn(0).setResizable(false);
            tabellaCommenti.getColumnModel().getColumn(1).setResizable(false);
            tabellaCommenti.getColumnModel().getColumn(2).setResizable(false);
        }

        saluto1.setText("Recenzioni e commenti dei nostri visitatori!");

        insertText.setText("Inserisci qui il testo da lasciare nella sezione commenti: ");

        maxValue.setFont(new java.awt.Font("Lucida Grande", 0, 8)); // NOI18N
        maxValue.setText("Siete pregati di non inserire volgarità, i trasgressori verranno stuprati. (max 500 char)");

        dislike.setText("DISLIKE");
        dislike.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dislikeActionPerformed(evt);
            }
        });

        like.setSelected(true);
        like.setText("LIKE");

        insert.setText("Inserisci");
        insert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        back.setText("<<");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(Titolo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(saluto1))
                    .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(insertText)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(maxValue)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(insert, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(122, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dislike, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(like, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(saluto1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(insertText, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(like)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dislike))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(maxValue, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(insert, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(67, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void dislikeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dislikeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dislikeActionPerformed

    //inserimento di un nuovo commento
    private void insertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertActionPerformed
        //se il testo supera i 500 char oppure è vuoto allora si effettua un refresh
        if ("".equals(jTextArea1.getText()) || jTextArea1.getText().length() >= 500) {
            try {
                this.setVisible(false);
                GUI_Commenti com= new GUI_Commenti(1);
                com.setVisible(true);
            } catch (IOException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            }
            //sennò vengono ottenunti gli input dell'utente ed effettuata la richiesta post per un nuovo commento
        }else{            
            try {
                //ottengo il parametro LIKE o DISLIKE
                if(like.isSelected()){
                    feel=like.getText();
                }
                else if(dislike.isSelected()){
                    feel=dislike.getText();
                }
                //ottengo il testo
                String testo = jTextArea1.getText();                
                request comm = new request();
                JSONObject json =new JSONObject();
                json.put("id",conf.id);
                json.put("feel",feel);
                json.put("testo",testo);
                //invio la richiesta post al server
                comm.postRequest(conf.server, conf.port, "comment", json);
                try {
                    this.setVisible(false);
                    GUI_Utenti usr= new GUI_Utenti(1, 1, conf.pin);
                    usr.setVisible(true);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalBlockSizeException ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(GUI_Commenti.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_insertActionPerformed

    //ritorna al frame precedente 
    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        try {
            this.setVisible(false);
            GUI_Utenti gui = new GUI_Utenti(0, 1, conf.pin);
            gui.setVisible (true);
        } catch (ParseException | IOException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_backActionPerformed

    private void tabellaCommentiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabellaCommentiMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tabellaCommentiMouseEntered

    
    private String feel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton back;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton dislike;
    private javax.swing.JButton insert;
    private javax.swing.JLabel insertText;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JRadioButton like;
    private javax.swing.JLabel maxValue;
    private javax.swing.JLabel saluto1;
    private javax.swing.JTable tabellaCommenti;
    // End of variables declaration//GEN-END:variables
}
