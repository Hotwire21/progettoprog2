package clientmuseo;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class ClientMuseo { 
    /* private static final int APIKEY = 0; // your api key
    private static final String APIHASH = "<your-api-hash"; // your api hash
    private static final String PHONENUMBER = "<your-phone-number>"; // Your phone number
    public static final String BASEURL = "https://api.telegram.org/bot";
    public static final String TOKEN = "780811258:AAHTLELbko2xhCDcrbdb4za3Xp8DPfy-Jj4";
    public static final String PATH = "/getChat?";
    public static final String RESOURCE1 = "chat_id=-222349245&force_reply=true";
    public static final String RESOURCE = "text=/start&chat_id=208658864&disable_notification=true&force_reply=true";*/
    
    
    public static void main(String[] args) throws MalformedURLException, ParseException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, Exception {
        
        //Creazione chiavi di crittografia pubblica e privata            
        GenerateKeys gk;
        try {
            gk = new GenerateKeys(1024);
            gk.createKeys();
            gk.writeToFile(conf.dir+"publicKey", gk.getPublicKey().getEncoded());
            gk.writeToFile(conf.dir+"privateKey", gk.getPrivateKey().getEncoded()); 
            
            /*String url = BASEURL + TOKEN + PATH+ RESOURCE1;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int dim_Response = con.getContentLength();            
            DataInputStream in2 = new DataInputStream(con.getInputStream());                                    
            byte[] data = new byte[dim_Response];     
            for(int i=0;i<11;i++){
                System.out.print(con.getHeaderFieldKey(i)+"  --  ");
                System.out.println(con.getHeaderField(i));
            }
            System.out.println("prova:  "+con.getResponseMessage());
            in2.readFully(data);
            in2.close();
            System.out.print(new String(data));
            
            
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
            }
            in.close();*/
            //System.out.println(response);

          
            
            
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        //Invio richiesta al server per verificare la connessione
        URL obj = new URL(conf.server + ":" + conf.port + "/testConnessione");
        int a=0;
            try {
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();                
                con.setRequestMethod("GET");
                con.setRequestProperty("Accept-Language", "en-US");
                con.setRequestProperty("Content-Type", "application/json");                
                con.setDoOutput(true);                
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.write("".getBytes());
                wr.flush();
                wr.close();
                //Se il server è online procedo
                if (con.getResponseCode() == 200) {
                    a=1;
                    exe(a);
                    keys();
                    GUI_Login gui = new GUI_Login();
                    gui.setVisible(true);                         
                }
            //Se il server è offline termino il programma
            } catch (IOException ex) {
                exe(a);
                ex.printStackTrace();
            }            
                         
        
    }
    
    static public void keys() throws MalformedURLException, ProtocolException, ProtocolException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{           
        //Richiesta al server della sua chiave pubblica
        URL obj1 = new URL(conf.server + ":" + conf.port + "/publicKey");            
        HttpURLConnection con1 = (HttpURLConnection) obj1.openConnection();                
        con1.setRequestMethod("GET");
        con1.setRequestProperty("Accept-Language", "en-US");             
        con1.setDoOutput(true);                
        DataOutputStream wr1 = new DataOutputStream(con1.getOutputStream());
        wr1.write("".getBytes());
        wr1.flush();
        wr1.close();
        //ottengo la risposta
        int dim_Response = con1.getContentLength();            
        DataInputStream in = new DataInputStream(con1.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        in.close();
        //scrive la chiave su di un file
        AsymmetricCryptography ac = new AsymmetricCryptography();
        File f =new File(conf.dir+"serverKey");
        ac.writeToFile(f, data);
 
        //Richiesta al server per postare la propria chiave pubblica
        URL obj2 = new URL(conf.server + ":" + conf.port + "/getClientKey" );            
        HttpURLConnection con2 = (HttpURLConnection) obj2.openConnection();
        con2.setRequestMethod("POST");
        con2.setRequestProperty("Accept-Language", "en-US");
        //prende la chiave dal file e la invia al server
        File f2 =new File(conf.dir+"publicKey");
        byte[] myKey=ac.getFileInBytes(f2);
        con2.setDoOutput(true);
        DataOutputStream wr2 = new DataOutputStream(con2.getOutputStream());
        wr2.write(myKey);
        wr2.flush();
        wr2.close();                                                
        int dim_Response2 = con2.getContentLength();            
        DataInputStream in2 = new DataInputStream(con2.getInputStream());                                    
        byte[] data2 = new byte[dim_Response2];            
        in2.readFully(data2);
        in2.close();  
        
    }
            
    //creazione di un frame usato per verificare la connessione che mostra una barra di avanzamento stato
    static public void exe (int a) throws FileNotFoundException, IOException, ParseException{                                                                                                                        
        final int MAX = 100;
        final JFrame frame = new JFrame("Museo di Messina");
        frame.setResizable(false);
        // creo progress bar
        final JProgressBar pb = new JProgressBar();
        pb.setMinimum(0);
        pb.setMaximum(MAX);
        pb.setStringPainted(true);
 
        // aggiungo la progress bar al frame
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(pb);       
        
        JLabel testo=new JLabel();
        frame.getContentPane().add(testo);
 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 80);
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);        
        frame.setVisible(true);                
        
        // aggiorno progress bar
        for (int i = 0; i <= MAX; i++) {
            final int currentValue = i;
            int wait = 40;
            if (currentValue == 0){
                pb.setVisible (false);
                testo.setText ("Avvio del programma in corso...");
                wait = 2000;
            }
            if (currentValue == 1){
                pb.setVisible (true);
                testo.setText ("Caricamento informazioni...");
                }
            if (currentValue == 39){
                testo.setText ("Controllo connessione...");
                wait = 1500;
            }
            //la var a serve per la verifica dello stato della connessione
            if (currentValue == 61){
                if(a==0){
                    testo.setText ("Server offline! Terminazione programma!");
                    wait = 1000;                        
                }else{
                    testo.setText ("Caricamento impostazioni utente...");
                    wait = 700;
                }
            }
            if (currentValue == 99){
                if(a==0){
                    System.exit(0);                        
                }else{
                    testo.setText ("Caricamento GUI in corso...");
                    wait = 1000;
                }
            }                                              
            try {
                SwingUtilities.invokeLater(() -> {
                    pb.setValue(currentValue);
                });
                java.lang.Thread.sleep(wait);
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(frame, e.getMessage());
            }            
        } 
        frame.setVisible(false);
    }
}
