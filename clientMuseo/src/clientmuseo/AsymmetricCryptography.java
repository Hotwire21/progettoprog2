package clientmuseo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.codec.binary.Base64;

public class AsymmetricCryptography {
	
    private Cipher cipher;
    
    public AsymmetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.cipher = Cipher.getInstance("RSA");
    }

    //ottiene l'oggetto chiave privata dal file
    public PrivateKey getPrivate(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }
    //ottiene l'oggetto chiave pubblica dal file
    public PublicKey getPublic(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }
    //scrive un byte array su file
    public void writeToFile(File output, byte[] toWrite) throws IllegalBlockSizeException, BadPaddingException, IOException {
        FileOutputStream fos = new FileOutputStream(output);
        fos.write(toWrite);
        fos.flush();
        fos.close();
    }
    
    //Cifratura di una stringa
    public String encryptText(String msg, PublicKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        this.cipher.init(Cipher.ENCRYPT_MODE, key);
        String[] msg1=new String[100];
        String finale = "";
        //se la lunghezza della stringa supera la capacità della chiave allora si procede suddividendola in blocchi
        if (msg.length()>115){
            int a=(msg.length()/116)+1;
            //ciclo di creazione dei blocchi di codice in base64 e successivamente codificati con la chiave
            for(int i=0;i<a;i++)
            {   
                if (msg.length()>(i+1)*115){
                    msg1[i]=msg.substring(i*115,115*(i+1));
                    finale=finale+Base64.encodeBase64String(cipher.doFinal(msg1[i].getBytes("UTF-8")));
                    //System.out.println(msg1[i]);
                }else{
                    msg1[i]=msg.substring(i*115,msg.length());
                    //System.out.println(msg1[i]);
                    finale=finale+Base64.encodeBase64String(cipher.doFinal(msg1[i].getBytes("UTF-8")));
                }
            }
        //se la lunghezza è inferiore alla capacità allora ci sarà una sola stringa
        }else{                    
            finale=Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
        }
    return finale;
    }

    //Decifratura di una stringa
    public String decryptText(String msg, PrivateKey key)throws InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        this.cipher.init(Cipher.DECRYPT_MODE, key);
        String[] msg1=new String[100];
        String[] msg2=new String[100];
        //ottengo il numero delle stringhe da decifrare
        long count = msg.chars().filter(ch -> ch == '=').count();
        String temp="";
        //ciclo per la suddivisione delle stringhe con successiva decodifica
        for(int i=0;i<count;i++){
            if (i==0){
                msg1[i]=msg.substring(0, msg.indexOf("="));
                temp=msg.substring( (msg.indexOf("=")+1) );
            }else{                        
                msg1[i]=temp.substring(0, temp.indexOf("="));
                temp=temp.substring( (msg.indexOf("=")+1) );
            }
            msg2[i]=new String(cipher.doFinal(Base64.decodeBase64(msg1[i])), "UTF-8");                   
        }
        //accorpamento della stringa originale decodificata
        String last="";
        for(int j=0;j<count;j++){
            last=last+msg2[j];
        }
        return last;
    }
    
    //ottengo la chiave come byte array
    public byte[] getFileInBytes(File f) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] fbytes = new byte[(int) f.length()];
        fis.read(fbytes);
        fis.close();
        return fbytes;
    }
		
}