package clientmuseo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class request {
    //effettua la richiesta post al server criptando la string ada inviare e decriptando la risposta
    public JSONObject postRequest(String address, String port, String page, JSONObject json) throws MalformedURLException, ProtocolException, IOException, ParseException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, Exception {   
        //ottiene le chiavi dai file
        AsymmetricCryptography ac = new AsymmetricCryptography();
        PublicKey serverKey= ac.getPublic(conf.dir+"serverKey");
        PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
        //istanzia la richiesta con i rispettivi header e la invia
        URL obj = new URL(address + ":" + port + "/" + page);            
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        // cifra la stringa(JSON) da inviare al server
        String str =ac.encryptText(json.toString(), serverKey);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(str.getBytes());
        wr.flush();
        wr.close();                                                    
        
        // decifra la risposta del server
        int dim_Response = con.getContentLength();            
        DataInputStream in = new DataInputStream(con.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        in.close();
        String str1=ac.decryptText(new String(data), privKey);

        //converte la stringa in un oggetto json
        JSONParser parser = new JSONParser();            
        JSONObject returnJson = (JSONObject) parser.parse(str1);            
        return returnJson;
    }
    
    //effettua la richiesta get al server criptando la string ada inviare e decriptando la risposta
    public JSONObject getRequest(String address, String port, String page) throws MalformedURLException, ProtocolException, IOException, ParseException, NoSuchAlgorithmException, NoSuchPaddingException, Exception {   
        //ottiene le chiavi dai file    
        AsymmetricCryptography ac = new AsymmetricCryptography();
        PublicKey serverKey= ac.getPublic(conf.dir+"serverKey");
        PrivateKey privKey= ac.getPrivate(conf.dir+"privateKey");
        //istanzia la richiesta con i rispettivi header e la invia
        URL obj = new URL(address + ":" + port + "/" + page);            
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();     
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept-Language", "en-US");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);                
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        // cifra la stringa da inviare al server            
        String str =ac.encryptText(page, serverKey);
        wr.write(str.getBytes());
        wr.flush();
        wr.close();                                    

        // decifra la risposta del server
        int dim_Response = con.getContentLength();            
        DataInputStream in = new DataInputStream(con.getInputStream());                                    
        byte[] data = new byte[dim_Response];            
        in.readFully(data);
        in.close();
        String str1=ac.decryptText(new String(data), privKey);
        //converte la stringa in un oggetto json
        JSONParser parser = new JSONParser();
        JSONObject returnJson = (JSONObject) parser.parse(str1);
        return returnJson;
    }
} 

