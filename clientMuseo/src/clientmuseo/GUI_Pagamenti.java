package clientmuseo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.ButtonGroup;
import org.json.simple.JSONObject;

import org.json.simple.parser.ParseException;

public class GUI_Pagamenti extends javax.swing.JFrame {
    
    public GUI_Pagamenti() throws MalformedURLException, ProtocolException, IOException {
        //inizializzazione dei componenti della GUI
        initComponents();  
        ButtonGroup bg = new ButtonGroup();
            bg.add(bitcoin);
            bg.add(maestro);
            bg.add(postepay);
            bg.add(visa);
            bg.add(paypal);
            bg.add(mastercard);
            paypal.setContentAreaFilled(true);
            endButton.setVisible(false);
            end1.setVisible(false);
            end2.setVisible(false);
            codice.setVisible(false);
            ok.setVisible(false);
            jLabel7.setVisible(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        paypal = new javax.swing.JRadioButton();
        visa = new javax.swing.JRadioButton();
        bitcoin = new javax.swing.JRadioButton();
        mastercard = new javax.swing.JRadioButton();
        maestro = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        postepay = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        telefono = new javax.swing.JTextField();
        start1 = new javax.swing.JLabel();
        start2 = new javax.swing.JLabel();
        end1 = new javax.swing.JLabel();
        end2 = new javax.swing.JLabel();
        codice = new javax.swing.JTextField();
        ok = new javax.swing.JLabel();
        startButton = new javax.swing.JToggleButton();
        endButton = new javax.swing.JToggleButton();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        paypal.setSelected(true);
        paypal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paypalActionPerformed(evt);
            }
        });

        visa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visaActionPerformed(evt);
            }
        });

        bitcoin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitcoinActionPerformed(evt);
            }
        });

        mastercard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mastercardActionPerformed(evt);
            }
        });

        maestro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maestroActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/paypal.png"))); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/visa.png"))); // NOI18N

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/bitcoin.png"))); // NOI18N

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/Master-Card.png"))); // NOI18N

        postepay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                postepayActionPerformed(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/Maestro.png"))); // NOI18N

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projectFiles/pay/postepay.png"))); // NOI18N

        telefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telefonoActionPerformed(evt);
            }
        });

        start1.setText("Inserisci il tuo indirizzo email  ");

        start2.setText("per confermare il pagamento !");

        end1.setText("Inserisci il codice ricevuto per");

        end2.setText("terminare il pagamento");

        codice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codiceActionPerformed(evt);
            }
        });

        ok.setForeground(new java.awt.Color(255, 0, 0));
        ok.setText("IL CODICE INSERITO E' ERRATO RITENTA SARAI PIU' FORTUNATO!");

        startButton.setText("Conferma");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        endButton.setText("Conferma");
        endButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                endButtonActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 51));
        jLabel7.setText("Devi riempire il campo!!!!!!!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(Titolo))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(ok)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addGap(18, 18, 18)
                                    .addComponent(postepay))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(visa))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(18, 18, 18)
                                    .addComponent(paypal))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addGap(18, 18, 18)
                                    .addComponent(mastercard))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addGap(18, 18, 18)
                                    .addComponent(maestro))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(18, 18, 18)
                                    .addComponent(bitcoin)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(56, 56, 56)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(57, 57, 57))
                                        .addComponent(start1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(start2, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(telefono)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(83, 83, 83)
                                    .addComponent(jLabel7))))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(105, 105, 105)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(end1)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(17, 17, 17)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(end2)
                                        .addComponent(codice, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(34, 34, 34)
                                    .addComponent(endButton, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(97, 97, 97))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(paypal)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(postepay)
                            .addComponent(jLabel6))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(visa)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mastercard))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(maestro)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bitcoin)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(start1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(start2)
                        .addGap(9, 9, 9)
                        .addComponent(telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addGap(98, 98, 98)))
                .addGap(20, 20, 20)
                .addComponent(end1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(end2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(codice, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endButton, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(ok)
                .addGap(132, 132, 132))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void paypalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paypalActionPerformed

    }//GEN-LAST:event_paypalActionPerformed

    private void visaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visaActionPerformed
        
    }//GEN-LAST:event_visaActionPerformed

    private void bitcoinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bitcoinActionPerformed
        
    }//GEN-LAST:event_bitcoinActionPerformed

    private void mastercardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mastercardActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mastercardActionPerformed

    private void maestroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maestroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_maestroActionPerformed

    private void postepayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_postepayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_postepayActionPerformed

    private void telefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_telefonoActionPerformed

    private void codiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codiceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_codiceActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if ("".equals(telefono.getText())){
            jLabel7.setVisible(true);
        }else{
            try {
                jLabel1.setVisible(false);
                jLabel2.setVisible(false);
                jLabel3.setVisible(false);
                jLabel4.setVisible(false);
                jLabel5.setVisible(false);
                jLabel6.setVisible(false);
                jLabel7.setVisible(false);
                bitcoin.setVisible(false);
                maestro.setVisible(false);
                mastercard.setVisible(false);
                paypal.setVisible(false);
                postepay.setVisible(false);
                visa.setVisible(false);
                start1.setVisible(false);
                start2.setVisible(false);
                startButton.setVisible(false);
                telefono.setVisible(false);
                end1.setVisible(true);
                end2.setVisible(true);
                endButton.setVisible(true);
                codice.setVisible(true);
                JSONObject obj = new JSONObject();
                obj.put("tel", telefono.getText());
                request req = new request();
                req.postRequest(conf.server, conf.port, "payServer", obj);
            } catch (IOException ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_startButtonActionPerformed

    private void endButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_endButtonActionPerformed
        try {
            JSONObject returnJson = new JSONObject();
            JSONObject obj = new JSONObject();
            obj.put("cod", codice.getText());
            request req = new request();
            returnJson=req.postRequest(conf.server, conf.port, "validatePayServer", obj);
            String ris=(String)returnJson.get("ok");
            if("1".equals(ris)){
                this.setVisible(false);
                GUI_Utenti gui = new GUI_Utenti(3, 1, "");
                gui.setVisible (true);
            }else{
                ok.setVisible(true);
                codice.setText("");
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Pagamenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_endButtonActionPerformed
       
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JRadioButton bitcoin;
    private javax.swing.JTextField codice;
    private javax.swing.JLabel end1;
    private javax.swing.JLabel end2;
    private javax.swing.JToggleButton endButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JRadioButton maestro;
    private javax.swing.JRadioButton mastercard;
    private javax.swing.JLabel ok;
    private javax.swing.JRadioButton paypal;
    private javax.swing.JRadioButton postepay;
    private javax.swing.JLabel start1;
    private javax.swing.JLabel start2;
    private javax.swing.JToggleButton startButton;
    private javax.swing.JTextField telefono;
    private javax.swing.JRadioButton visa;
    // End of variables declaration//GEN-END:variables
   
    
}
