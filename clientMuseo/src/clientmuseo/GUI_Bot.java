package clientmuseo;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.json.simple.parser.ParseException;

public class GUI_Bot extends javax.swing.JFrame {
    
    public GUI_Bot() throws Exception{
        initComponents();
        jComboBox1.addItem("/start");
        jLabel8.setForeground(Color.red);
        jLabel8.setText("BOT spento");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        Titolo1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        back = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        Titolo1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        Titolo1.setForeground(new java.awt.Color(0, 102, 255));
        Titolo1.setText("@mume_bot");
        Titolo1.setToolTipText("");
        Titolo1.setAlignmentY(1.0F);

        jLabel1.setText("Sezione dedicata al bot di assistenza per i clienti che utilizzano l'applicazione");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)))
                .addContainerGap(338, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(10, 10, 10)
                .addComponent(jLabel4)
                .addGap(10, 10, 10)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addContainerGap(287, Short.MAX_VALUE))
        );

        jButton1.setText(">");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel7.setText("per qualsiasi info basta chiedere seguendo i comandi!");

        jLabel8.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setText("jLabel8");

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("E' possibile anche interagire tramite l'applicazione di telegram aggiungendo @mume_bot");

        back.setText("<<");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(Titolo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 594, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(364, 364, 364)
                .addComponent(Titolo1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(61, 61, 61)
                .addComponent(Titolo1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addGap(28, 28, 28))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            String ok=(String) jComboBox1.getSelectedItem();
            switch (ok){
                case "/start":
                    itemAdd((String) jComboBox1.getSelectedItem());
                    jLabel2.setText("/foto - Foto di una vista");
                    jLabel3.setText("/eventi - Lista prossimi eventi");
                    jLabel4.setText("/posizione - Posizione su google maps");
                    jLabel5.setText("/promozioni - Possibili sconti e promozioni");
                    jLabel6.setText("/stop - Spegnimento bot");
                    jLabel8.setForeground(Color.green);
                    jLabel8.setText("START");
                    break;
                case "/foto":
                    itemAdd((String) jComboBox1.getSelectedItem());
                    Image image=null;
                    URL url = new URL("http://mume.altervista.org/img/foto_min/foto1.jpg");
                    image = ImageIO.read(url);
                    jLabel2.setIcon(new ImageIcon(image));
                    jLabel2.setText("");
                    jLabel3.setText("Qui potrai visualizzare l'intera galleria di foto");
                    jLabel3.addMouseListener(new MouseAdapter() {
                        public void mousePressed(MouseEvent me){
                            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                                try {
                                    desktop.browse(new URI("http://mume.altervista.org/gallery.php"));            
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (URISyntaxException ex) {
                                    Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    });
                    jLabel4.setText("");
                    jLabel5.setText("");
                    jLabel6.setText("");
                    jLabel8.setText("FOTO");
                    break;
                case "/eventi":
                    itemAdd((String) jComboBox1.getSelectedItem());
                    jLabel2.setText("Mostra Caravaggio - 25/12/2018");
                    jLabel2.setForeground(Color.LIGHT_GRAY);
                    jLabel2.addMouseListener(new MouseAdapter() {
                         public void mousePressed(MouseEvent me){
                             try {
                                 redir(2018,12,25);
                             } catch (NoSuchPaddingException ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             } catch (Exception ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             }
                         }
                    });
                    jLabel3.setText("Convegno di archeologia classica - 01/01/2019");
                    jLabel3.setForeground(Color.LIGHT_GRAY);
                    jLabel3.addMouseListener(new MouseAdapter() {
                         public void mousePressed(MouseEvent me1){
                             try {
                                 redir(2019,1,1);
                             } catch (NoSuchPaddingException ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             } catch (Exception ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             }
                         }
                    });
                    jLabel4.setText("Seminario agiografia - 07-01-2019");
                    jLabel4.setForeground(Color.LIGHT_GRAY);
                    jLabel4.addMouseListener(new MouseAdapter() {
                         public void mousePressed(MouseEvent me2){
                             try {
                                 redir(2019,1,7);
                             } catch (NoSuchPaddingException ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             } catch (Exception ex) {
                                 Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                             }
                         }
                    });
                    jLabel5.setText("Cliccando sull'evento è possibile prenotarsi");
                    jLabel6.setText("");
                    jLabel8.setText("EVENTI");
                    break;
                case "/posizione":
                    itemAdd((String) jComboBox1.getSelectedItem());
                    jLabel2.addMouseListener(new MouseAdapter() {
                        public void mousePressed(MouseEvent me){
                            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                                try {
                                    desktop.browse(new URI("http://bit.ly/2T5sJUJ"));            
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (URISyntaxException ex) {
                                    Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    });
                    jLabel2.setText("Apri mappa su GoogleMaps");
                    jLabel2.setForeground(Color.LIGHT_GRAY);
                    jLabel3.setText("");
                    jLabel4.setText("");
                    jLabel5.setText("");
                    jLabel6.setText("");
                    jLabel8.setText("POSIZIONE");
                    break;
                case "/promozioni":
                    itemAdd((String) jComboBox1.getSelectedItem());
                    jLabel2.setText("Fine settimana -10% su tutte le prenotazioni");
                    jLabel3.setText("Gruppo minimo 10 persone 30% sconto");
                    jLabel4.setText("Guida per gruppo min 5 persone - 20%");
                    jLabel5.setText("");
                    jLabel6.setText("");
                    jLabel8.setText("PROMOZIONI");
                    break;
                case "/stop":
                    stopClear();
                    jComboBox1.removeAllItems();
                    jComboBox1.addItem("/start");
                    jLabel8.setForeground(Color.red);
                    jLabel8.setText("BOT spento");
                    break;
                default:
                    System.out.print(ok); 
            }
        } catch (IOException ex) {
            Logger.getLogger(GUI_Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel2MouseClicked

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        try {
            this.setVisible(false);
            GUI_Utenti gui = new GUI_Utenti(0, 1, conf.pin);
            gui.setVisible (true);
        } catch (ParseException | IOException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_backActionPerformed
    private void redir(int y,int m,int d) throws ParseException, NoSuchPaddingException, Exception{
        Calendar myCal = Calendar.getInstance();
        myCal.set(Calendar.YEAR, y);
        myCal.set(Calendar.MONTH, m);
        myCal.set(Calendar.DAY_OF_MONTH, d);
        Date Date = myCal.getTime();
        this.setVisible(false);
        GUI_Prenotazioni pr= new GUI_Prenotazioni("", Date);
        pr.setVisible(true);
    }
    public void stopClear(){
        jLabel2.setIcon(null);
        jLabel2.removeAll();
        jLabel2.setText("");
        jLabel3.setText("");
        jLabel4.setText("");
        jLabel5.setText("");
        jLabel6.setText("");
        jLabel8.setText("");
    }
    public void itemAdd(String sel){
        jLabel2.setIcon(null);
        jLabel2.removeAll();
        jLabel2.setForeground(Color.BLACK);
        jLabel3.setForeground(Color.BLACK);
        jLabel3.removeAll();
        jLabel4.setForeground(Color.BLACK);
        jLabel4.removeAll();
        jLabel5.setForeground(Color.BLACK);
        jLabel5.removeAll();
        jLabel6.setForeground(Color.BLACK);
        jLabel6.removeAll();
        jComboBox1.removeAllItems();
        jComboBox1.addItem("/foto");
        jComboBox1.addItem("/eventi");
        jComboBox1.addItem("/posizione");
        jComboBox1.addItem("/promozioni");
        jComboBox1.addItem("/stop");
        jComboBox1.removeItem(sel);  
    }
    
          

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JLabel Titolo1;
    private javax.swing.JButton back;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
