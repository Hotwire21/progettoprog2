package clientmuseo;

import com.sun.jndi.toolkit.url.Uri;
import java.awt.Desktop;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.simple.parser.ParseException;

public class GUI_Conferma extends javax.swing.JFrame {
private URI uri = null;
    /**
     * Creates new form GUI_Conferma
     * @param a
     * @throws java.net.URISyntaxException
     */
    public GUI_Conferma(String a) throws URISyntaxException {        
        //inizializzazione dei componenti della GUI
        uri = new URI(a);
        initComponents(); 
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        saluto2 = new javax.swing.JLabel();
        saluto3 = new javax.swing.JLabel();
        saluto4 = new javax.swing.JLabel();
        saluto5 = new javax.swing.JLabel();
        saluto6 = new javax.swing.JLabel();
        saluto7 = new javax.swing.JLabel();
        saluto8 = new javax.swing.JLabel();
        saluto9 = new javax.swing.JLabel();
        download = new javax.swing.JButton();
        home = new javax.swing.JButton();
        pay = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        saluto2.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        saluto2.setText("LA PRENOTAZIONE E' AVVENUTA CON SUCCESSO !");

        saluto3.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto3.setText("CLICCANDO SUL BOTTONE DOWNLOAD PUOI SCARICARE IL PDF CONTENTENTE IL");

        saluto4.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto4.setText("PDF CONTENTENTE IL RIEPILOGO DELLA TUA PRENOTAZIONE CHE DOVRA' ESSERE ");

        saluto5.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto5.setText("PRESENTATO AL MOMENTO DELL'ACQUISTO IL BIGLIETTERIA PRESSO IL MUSEO!");

        saluto6.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto6.setText("QUANDO IL SISTEMA RILEVERA' LA TUA 5 PRENOTAZIONE VERRA' APPLICATO UNO");

        saluto7.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto7.setText("UN BUONO SCONTO DEL 10% SUL PREZZO TOTALE DEI BIGLIETTI !");

        saluto8.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        saluto8.setText("GRAZIE PER AVER USUFRUITO DEI NOSTRI SERVIZI.  BUONA GIORNATA!");

        saluto9.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        saluto9.setText("ATTENZIONE ! ! !");

        download.setText("Download PDF");
        download.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadActionPerformed(evt);
            }
        });

        home.setText("Home");
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        pay.setText("Paga Ora");
        pay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(saluto2)
                        .addGap(56, 56, 56))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(saluto9, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(203, 203, 203))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(Titolo)
                        .addGap(135, 135, 135))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(download, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(pay, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(saluto8, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54))))
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(saluto3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saluto4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saluto5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saluto6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(saluto7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(saluto2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saluto3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saluto4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saluto5)
                .addGap(46, 46, 46)
                .addComponent(saluto9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saluto6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saluto7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 109, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(download, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pay, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(91, 91, 91)
                .addComponent(saluto8, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void downloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadActionPerformed
        //alla pressione del tasto download verrà effettuato il download del pdf all'interno del broswer
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);            
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_downloadActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        //alla pressione del tasto si ritorna alla home page
        try {
            this.setVisible(false);
            GUI_Utenti home = new GUI_Utenti(0, 1, conf.pin);
            home.setVisible(true);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    } catch (NoSuchPaddingException ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IllegalBlockSizeException ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    } catch (Exception ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    }//GEN-LAST:event_homeActionPerformed

    private void payActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payActionPerformed

    try {
        this.setVisible(false);
        GUI_Pagamenti pay = new GUI_Pagamenti();
        pay.setVisible(true);
    } catch (ProtocolException ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
        Logger.getLogger(GUI_Conferma.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    }//GEN-LAST:event_payActionPerformed

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton download;
    private javax.swing.JButton home;
    private javax.swing.JButton pay;
    private javax.swing.JLabel saluto2;
    private javax.swing.JLabel saluto3;
    private javax.swing.JLabel saluto4;
    private javax.swing.JLabel saluto5;
    private javax.swing.JLabel saluto6;
    private javax.swing.JLabel saluto7;
    private javax.swing.JLabel saluto8;
    private javax.swing.JLabel saluto9;
    // End of variables declaration//GEN-END:variables
}
