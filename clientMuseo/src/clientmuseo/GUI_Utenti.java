package clientmuseo;

import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_Utenti extends javax.swing.JFrame {
    
    public GUI_Utenti(int commento, int login, String pin) throws MalformedURLException, ParseException, FileNotFoundException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, Exception {
        //inizializzazione dei componenti della GUI
        initComponents();                        
        if (commento==0)
        {end.setVisible(false);}
        else if(commento==1)
        {end.setVisible(true);}
        else if(commento==3)
        {end.setText("Pagamento effettuato!");end.setVisible(true);}
        
        if (login==0){
            JSONObject obj = new JSONObject();
            obj.put("pin", pin);
            JSONObject returnJson = new JSONObject();            
            //invio richiesta al server
            request log = new request();
           
            returnJson = log.postRequest(conf.server, conf.port, "loginClient", obj);
            if("false".equals((String) returnJson.get("res"))){
                this.setVisible(false);  
                GUI_Login lo = new GUI_Login();
                lo.setVisible(true);
            }else{
            conf.nome = (String) returnJson.get("nome");
            conf.foto = (String) returnJson.get("foto");
            conf.id = (String) returnJson.get("id");
            utente.setText(conf.nome);
            Image image=null;
            URL url = new URL(conf.foto);
            image = ImageIO.read(url);
            pict.setIcon(new ImageIcon(image));        
            }
        }else{
            utente.setText(conf.nome);
            Image image=null;
            URL url = new URL(conf.foto);
            image = ImageIO.read(url);
            pict.setIcon(new ImageIcon(image));        
        }
        //ottengo dati dall'account twitter e li inserisce nelle variabili utilizzate nella GUI
                                
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        utente = new javax.swing.JLabel();
        pict = new javax.swing.JLabel();
        prenota = new javax.swing.JButton();
        recensioni = new javax.swing.JButton();
        end = new javax.swing.JLabel();
        telegram = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        jLabel1.setText("Accesso effettuato da:");

        prenota.setText("Prenota");
        prenota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prenotaActionPerformed(evt);
            }
        });

        recensioni.setText("Commenti");
        recensioni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recensioniActionPerformed(evt);
            }
        });

        end.setForeground(new java.awt.Color(255, 51, 51));
        end.setText("Commento inserito!");

        telegram.setText("Help Bot");
        telegram.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telegramActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(prenota, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(recensioni, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pict, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(utente, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(end)
                                .addGap(154, 154, 154))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(223, 223, 223)
                        .addComponent(telegram, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(90, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(end)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 103, Short.MAX_VALUE)
                .addComponent(pict, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(utente))
                .addGap(116, 116, 116)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prenota, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(recensioni, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addComponent(telegram, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(108, Short.MAX_VALUE))
        );

        utente.getAccessibleContext().setAccessibleName("utente");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void prenotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prenotaActionPerformed
        try {
            this.setVisible(false);
            GUI_Prenotazioni pre= new GUI_Prenotazioni("",null);   
            pre.setVisible(true);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_prenotaActionPerformed

    private void recensioniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recensioniActionPerformed
        try {
            this.setVisible(false);
            GUI_Commenti com= new GUI_Commenti(0);
            com.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_recensioniActionPerformed

    private void telegramActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telegramActionPerformed
        try {
            this.setVisible(false);
            GUI_Bot tel = new GUI_Bot();
            tel.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Utenti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_telegramActionPerformed
       
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JLabel end;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel pict;
    private javax.swing.JButton prenota;
    private javax.swing.JButton recensioni;
    private javax.swing.JButton telegram;
    private javax.swing.JLabel utente;
    // End of variables declaration//GEN-END:variables
   
    
}
