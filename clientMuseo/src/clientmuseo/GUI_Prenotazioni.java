package clientmuseo;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JPanel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXDatePicker;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class GUI_Prenotazioni extends javax.swing.JFrame {
    private int bambini=0;
    private int ridotti=0;
    private int adulti=0;
    private int tot=0;
    private String giorno="";

    public GUI_Prenotazioni(String error, Date bot) throws ProtocolException, IOException, MalformedURLException, ParseException, NoSuchPaddingException, Exception {
        //inizializzazione dei componenti della GUI
        initComponents();
        Date date= new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM");
        String strDate = dateFormat.format(date);
        dataS.setText(strDate);
        errore.setText(error);
        utente.setText(conf.nome);
        tabellaOrari.getTableHeader().setReorderingAllowed(false);
        request pre = new request();  
        //effettuo 2 richieste get una per gli orari ed una per i prezzi da stampare nella tabella e nel bottone
        JSONObject returnJson1 = pre.getRequest(conf.server, conf.port, "getOrari");
        JSONObject returnJson2 = pre.getRequest(conf.server, conf.port, "getPrezzi");
        JSONArray array = (JSONArray)returnJson1.get("giorni");
        //ciclo per la stampa degli orari ottenuti  
        for(int i=0;i<array.size();i++){
            JSONObject name = (JSONObject)array.get(i);
            JTableHeader th = tabellaOrari.getTableHeader();
            th.setBackground(Color.getHSBColor(0.6f, 0.6f, 0.6F ));
            TableColumnModel tcm = th.getColumnModel();
            TableColumn tc = tcm.getColumn(i);            
            tc.setHeaderValue(name.get("giorno"));            
            tabellaOrari.setValueAt(name.get("mattina"), 0, i);
            tabellaOrari.setValueAt(name.get("pomeriggio"), 1, i);
            tc.setResizable(false);
        }
        //ciclo per la stampa dei prezzi ottenuti
        JSONArray array1 = (JSONArray)returnJson2.get("prezzi");
        for(int i=0;i<array1.size();i++){
            JSONObject name1 = (JSONObject)array1.get(i);
            turni.addItem(String.valueOf(name1.get("nome"))+" €"+String.valueOf(name1.get("prezzo")));
        }
        //ciclo per la stampa delle possibilità di prenotazione
        int t=0;
        while(t<30){
            t++;
            quantita.addItem( Integer.toString(t));
        }
        //creazione del calendario per la scelta del giorno
        JPanel calendar = new JPanel();
        picker = new JXDatePicker();
        
        if (bot!=null){
            picker.setDate(bot);
        }else{
            picker.setDate(Calendar.getInstance().getTime());
        }
        calendar.setBounds(-60, 350, 300, 90);
        calendar.setPreferredSize(new Dimension(200, 40));
        picker.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
        calendar.add(picker);
        calendar.revalidate();
        calendar.repaint();
        add(calendar);
        
        //creazione tabella riepilogo
        String[]a={"Quantità","Tipo","Prezzo"};
        tabellaPrenot.getTableHeader().setReorderingAllowed(false);
        for(int i=0;i<3;i++){
            JTableHeader th = tabellaPrenot.getTableHeader();
            th.setBackground(Color.getHSBColor(0.6f, 0.6f, 0.6f));
            TableColumnModel tcm = th.getColumnModel();
            TableColumn tc = tcm.getColumn(i);
            tc.setHeaderValue(a[i]);            
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        canvas1 = new java.awt.Canvas();
        jButton1 = new javax.swing.JButton();
        Titolo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabellaOrari = new javax.swing.JTable();
        back = new javax.swing.JButton();
        saluto = new javax.swing.JLabel();
        utente = new javax.swing.JLabel();
        saluto1 = new javax.swing.JLabel();
        turni = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabellaPrenot = new javax.swing.JTable();
        quantita = new javax.swing.JComboBox<>();
        aggiungi = new javax.swing.JButton();
        cancella = new javax.swing.JButton();
        conferma = new javax.swing.JButton();
        saluto2 = new javax.swing.JLabel();
        errore = new javax.swing.JLabel();
        saluto3 = new javax.swing.JLabel();
        saluto4 = new javax.swing.JLabel();
        dataS = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Museo di Messina");
        setResizable(false);

        Titolo.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        Titolo.setForeground(new java.awt.Color(0, 102, 255));
        Titolo.setText("MUseo di MEssina");
        Titolo.setToolTipText("");
        Titolo.setAlignmentY(1.0F);

        tabellaOrari.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        tabellaOrari.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabellaOrari.setAutoscrolls(false);
        tabellaOrari.setEnabled(false);
        tabellaOrari.setFocusTraversalKeysEnabled(false);
        tabellaOrari.setFocusable(false);
        tabellaOrari.setRequestFocusEnabled(false);
        tabellaOrari.setRowSelectionAllowed(false);
        tabellaOrari.setShowHorizontalLines(false);
        tabellaOrari.setShowVerticalLines(false);
        tabellaOrari.getTableHeader().setResizingAllowed(false);
        tabellaOrari.getTableHeader().setReorderingAllowed(false);
        tabellaOrari.setUpdateSelectionOnSort(false);
        tabellaOrari.setVerifyInputWhenFocusTarget(false);
        jScrollPane1.setViewportView(tabellaOrari);

        back.setText("<<");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        saluto.setText("Gentile");

        saluto1.setText("è possibile prenotare solo dal giorno successivo al ");

        turni.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { }));
        turni.setLightWeightPopupEnabled(false);
        turni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                turniActionPerformed(evt);
            }
        });

        tabellaPrenot.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tabellaPrenot.setEnabled(false);
        jScrollPane2.setViewportView(tabellaPrenot);

        quantita.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { }));
        quantita.setLightWeightPopupEnabled(false);
        quantita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quantitaActionPerformed(evt);
            }
        });

        aggiungi.setText("+");
        aggiungi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aggiungiActionPerformed(evt);
            }
        });

        cancella.setText("Cancella");
        cancella.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancellaActionPerformed(evt);
            }
        });

        conferma.setText("Conferma");
        conferma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                confermaMousePressed(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                confermaMouseClicked(evt);
            }
        });
        conferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confermaActionPerformed(evt);
            }
        });

        saluto2.setText("n.b. è possibile effettuare solo una prenotazione giornaliera per utente.");

        errore.setForeground(new java.awt.Color(255, 0, 0));

        saluto3.setText("Aggiungi tutti i partecipanti e clicca su conferma!");

        saluto4.setText("scegli il tipo ed il numero dei partecipanti e clicca sul + per aggiungerli.");

        dataS.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(144, 144, 144)
                        .addComponent(errore))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(saluto2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(saluto)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(utente)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(saluto1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dataS))
                            .addComponent(saluto4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(43, 43, 43))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cancella, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(142, 142, 142)
                                .addComponent(conferma, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(96, 96, 96))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(saluto3)
                        .addGap(143, 143, 143))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(quantita, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(turni, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(aggiungi, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(Titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(saluto1)
                        .addComponent(dataS))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(saluto)
                        .addComponent(utente)))
                .addGap(1, 1, 1)
                .addComponent(saluto4)
                .addGap(1, 1, 1)
                .addComponent(saluto2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(turni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quantita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aggiungi, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 197, Short.MAX_VALUE)
                .addComponent(saluto3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancella, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(conferma, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(errore)
                .addGap(29, 29, 29))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        //alla pressione del tasto si ritorna alla home page
        try {
            this.setVisible(false);
            GUI_Utenti gui = new GUI_Utenti(0, 1, "");
            gui.setVisible (true);
        } catch (ParseException | IOException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_backActionPerformed

    private void turniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_turniActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_turniActionPerformed

    private void quantitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quantitaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quantitaActionPerformed

    private void aggiungiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aggiungiActionPerformed
        //alla pressione del tasto vengono estratti i valori nei campi di input ed inseriti nella tabella del riepilogo
        String t = turni.getSelectedItem().toString();
        String t2=t.replaceAll("([0-9, ,€])", "");
        String q = quantita.getSelectedItem().toString();        
	int q1 = Integer.parseInt(q);
        String t1=t.replaceAll("([a-z,A-Z, ,€])", "");
        int q2=Integer.parseInt(t1);        
        int tot=q1*q2;
        //int t1 = Integer.parseInt(t);
    
        tabellaPrenot.setValueAt(q, a,0);
        tabellaPrenot.setValueAt(t2, a,1);
        tabellaPrenot.setValueAt(tot,a,2);        
        a++;
    }//GEN-LAST:event_aggiungiActionPerformed

    private void cancellaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancellaActionPerformed
        //alla pressione del tasto viene ripulita la tabella del riepilogo
        a=0;
        for(int i=0;i<10;i++){
           for(int j=0;j<3;j++){              
            tabellaPrenot.setValueAt("", i,j);
           }
        }
        tot=0;
        bambini=0;
        ridotti=0;
        adulti=0;
    }//GEN-LAST:event_cancellaActionPerformed
    
    //alla pressione del tasto viene iniziata la procedura di prenotazione
    private void confermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confermaActionPerformed
        /*JFrame frame= new JFrame();
        frame.setResizable(false);
        frame.setSize(40, 80);
        JLabel label=new JLabel();
        ImageIcon r = new ImageIcon(conf.dir+"tenor.gif");
        label.setIcon(r);
        frame.add(label);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
        frame.setVisible(true);
        frame.validate();
        frame.revalidate();
        frame.repaint();
        Thread.sleep(3);
        */
        
        if (tabellaPrenot.getValueAt(0,0)==null){
            try {
                this.setVisible(false);
                GUI_Prenotazioni p= new GUI_Prenotazioni("Devi inserire almeno una persona",null);
                p.setVisible(true);
            } catch (ParseException ex) {
                Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            String[][] ar = new String [a][3];
            //Ciclo per l'inserimento dei valori presenti nel riepilogo all'interno delle corrette variabili
            for(int i=0;i<a;i++){
                for(int j=0;j<3;j++){
                    ar[i][j]=tabellaPrenot.getValueAt(i, j).toString();
                    if(null != ar[i][j])
                        switch (ar[i][j]) {
                            case "Bambino":
                                bambini=bambini+Integer.parseInt(ar[i][j-1]);
                                break;
                            case "Ridotto":
                                ridotti=ridotti+Integer.parseInt(ar[i][j-1]);
                                break;
                            case "Adulto":
                                adulti=adulti+Integer.parseInt(ar[i][j-1]);
                                break;
                            default:
                                break;
                        }
                }
                tot=tot+Integer.parseInt(ar[i][2]);
            }
            Date d = picker.getDate();
            Date oggi= new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            giorno=sdf.format(d);
            //creazione dell'oggetto json contenente tutti i valori necessari alla prenotazione
            JSONObject obj = new JSONObject();
            obj.put("nome", conf.nome);
            obj.put("id", conf.id);
            obj.put("data", giorno);
            obj.put("bambini", Integer.toString(bambini));
            obj.put("ridotti", Integer.toString(ridotti));
            obj.put("adulti", Integer.toString(adulti));
            obj.put("totale", tot);
            JSONObject returnJson = new JSONObject();
            request log = new request();
            
            //verifica della data che non sia antecedente al giorno successivo
            if(d.after(oggi)){
                try {
                    //effettua la richiesta di prenotazione in base alla risposta provvederà a effettuare le corrette operazioni
                    returnJson=log.postRequest(conf.server, conf.port, "prenota", obj);
                    if("errore".equals((String) returnJson.get("err")))
                    {
                        this.setVisible(false);
                        GUI_Prenotazioni pre=new GUI_Prenotazioni("Esiste già una prenotazione a tuo nome per questo giorno!",null);
                        pre.setVisible(true);
                    }else{
                        
                        this.repaint();
                        this.validate();
                        this.revalidate();
                        String a=(String) returnJson.get("link");
                        this.setVisible(false);
                        GUI_Conferma gui = new GUI_Conferma(a);
                        gui.setVisible (true);
                    }
                } catch (ProtocolException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalBlockSizeException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                try {
                    this.setVisible(false);
                    GUI_Prenotazioni pre=new GUI_Prenotazioni("Non puoi tornare nel passato Dr. Brown!",null);
                    pre.setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(GUI_Prenotazioni.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_confermaActionPerformed

    private void confermaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_confermaMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_confermaMouseClicked

    private void confermaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_confermaMousePressed
        
    }//GEN-LAST:event_confermaMousePressed
    
   
    private int a=0;
    private JXDatePicker picker;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton aggiungi;
    private javax.swing.JButton back;
    private javax.swing.JButton cancella;
    private java.awt.Canvas canvas1;
    private javax.swing.JButton conferma;
    private javax.swing.JLabel dataS;
    private javax.swing.JLabel errore;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox<String> quantita;
    private javax.swing.JLabel saluto;
    private javax.swing.JLabel saluto1;
    private javax.swing.JLabel saluto2;
    private javax.swing.JLabel saluto3;
    private javax.swing.JLabel saluto4;
    private javax.swing.JTable tabellaOrari;
    private javax.swing.JTable tabellaPrenot;
    private javax.swing.JComboBox<String> turni;
    private javax.swing.JLabel utente;
    // End of variables declaration//GEN-END:variables
}
